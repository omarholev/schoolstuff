﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Övning_3._11
{
    public partial class LimitedTextBox : UserControl
    {
        public LimitedTextBox()
        {
            InitializeComponent();
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            int numChars = 140 - textBox.Text.Length;
            lbl_NumChars.Text = "(" + numChars + ")";
        }
    }
}

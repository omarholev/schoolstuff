﻿namespace Övning_3._7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_Matcher = new System.Windows.Forms.DataGridView();
            this.dgv_Hemmalag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_Bortalag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_MålHemmaLag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_MålBortalag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Målrikast = new System.Windows.Forms.Button();
            this.tbx_Målrikast = new System.Windows.Forms.TextBox();
            this.btn_StörstaMålskillnad = new System.Windows.Forms.Button();
            this.tbx_Målskillnad = new System.Windows.Forms.TextBox();
            this.btn_FlyttaRadUp = new System.Windows.Forms.Button();
            this.btn_FlyttaRadNed = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Matcher)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_Matcher
            // 
            this.dgv_Matcher.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Matcher.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_Hemmalag,
            this.dgv_Bortalag,
            this.dgv_MålHemmaLag,
            this.dgv_MålBortalag});
            this.dgv_Matcher.Location = new System.Drawing.Point(13, 13);
            this.dgv_Matcher.Name = "dgv_Matcher";
            this.dgv_Matcher.Size = new System.Drawing.Size(615, 289);
            this.dgv_Matcher.TabIndex = 0;
            this.dgv_Matcher.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Matcher_CellEndEdit);
            this.dgv_Matcher.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgv_Matcher_UserAddedRow);
            this.dgv_Matcher.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgv_Matcher_UserDeletingRow);
            // 
            // dgv_Hemmalag
            // 
            this.dgv_Hemmalag.HeaderText = "Hemmalag";
            this.dgv_Hemmalag.Name = "dgv_Hemmalag";
            this.dgv_Hemmalag.Width = 200;
            // 
            // dgv_Bortalag
            // 
            this.dgv_Bortalag.HeaderText = "Bortalag";
            this.dgv_Bortalag.Name = "dgv_Bortalag";
            this.dgv_Bortalag.Width = 200;
            // 
            // dgv_MålHemmaLag
            // 
            this.dgv_MålHemmaLag.HeaderText = "Mål (H)";
            this.dgv_MålHemmaLag.Name = "dgv_MålHemmaLag";
            // 
            // dgv_MålBortalag
            // 
            this.dgv_MålBortalag.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_MålBortalag.HeaderText = "Mål (B)";
            this.dgv_MålBortalag.Name = "dgv_MålBortalag";
            // 
            // btn_Målrikast
            // 
            this.btn_Målrikast.Location = new System.Drawing.Point(13, 327);
            this.btn_Målrikast.Name = "btn_Målrikast";
            this.btn_Målrikast.Size = new System.Drawing.Size(162, 23);
            this.btn_Målrikast.TabIndex = 1;
            this.btn_Målrikast.Text = "Målrikast";
            this.btn_Målrikast.UseVisualStyleBackColor = true;
            this.btn_Målrikast.Click += new System.EventHandler(this.btn_Målrikast_Click);
            // 
            // tbx_Målrikast
            // 
            this.tbx_Målrikast.Location = new System.Drawing.Point(181, 329);
            this.tbx_Målrikast.Name = "tbx_Målrikast";
            this.tbx_Målrikast.ReadOnly = true;
            this.tbx_Målrikast.Size = new System.Drawing.Size(447, 20);
            this.tbx_Målrikast.TabIndex = 2;
            // 
            // btn_StörstaMålskillnad
            // 
            this.btn_StörstaMålskillnad.Location = new System.Drawing.Point(13, 357);
            this.btn_StörstaMålskillnad.Name = "btn_StörstaMålskillnad";
            this.btn_StörstaMålskillnad.Size = new System.Drawing.Size(162, 23);
            this.btn_StörstaMålskillnad.TabIndex = 3;
            this.btn_StörstaMålskillnad.Text = " Största Målskillnad";
            this.btn_StörstaMålskillnad.UseVisualStyleBackColor = true;
            this.btn_StörstaMålskillnad.Click += new System.EventHandler(this.btn_StörstaMålskillnad_Click);
            // 
            // tbx_Målskillnad
            // 
            this.tbx_Målskillnad.Location = new System.Drawing.Point(182, 359);
            this.tbx_Målskillnad.Name = "tbx_Målskillnad";
            this.tbx_Målskillnad.ReadOnly = true;
            this.tbx_Målskillnad.Size = new System.Drawing.Size(148, 20);
            this.tbx_Målskillnad.TabIndex = 4;
            // 
            // btn_FlyttaRadUp
            // 
            this.btn_FlyttaRadUp.Location = new System.Drawing.Point(635, 13);
            this.btn_FlyttaRadUp.Name = "btn_FlyttaRadUp";
            this.btn_FlyttaRadUp.Size = new System.Drawing.Size(75, 23);
            this.btn_FlyttaRadUp.TabIndex = 5;
            this.btn_FlyttaRadUp.Text = "Flytta Up";
            this.btn_FlyttaRadUp.UseVisualStyleBackColor = true;
            this.btn_FlyttaRadUp.Click += new System.EventHandler(this.btn_FlyttaRadUp_Click);
            // 
            // btn_FlyttaRadNed
            // 
            this.btn_FlyttaRadNed.Location = new System.Drawing.Point(635, 42);
            this.btn_FlyttaRadNed.Name = "btn_FlyttaRadNed";
            this.btn_FlyttaRadNed.Size = new System.Drawing.Size(75, 23);
            this.btn_FlyttaRadNed.TabIndex = 6;
            this.btn_FlyttaRadNed.Text = "Flytta Ned";
            this.btn_FlyttaRadNed.UseVisualStyleBackColor = true;
            this.btn_FlyttaRadNed.Click += new System.EventHandler(this.btn_FlyttaRadNed_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_FlyttaRadNed);
            this.Controls.Add(this.btn_FlyttaRadUp);
            this.Controls.Add(this.tbx_Målskillnad);
            this.Controls.Add(this.btn_StörstaMålskillnad);
            this.Controls.Add(this.tbx_Målrikast);
            this.Controls.Add(this.btn_Målrikast);
            this.Controls.Add(this.dgv_Matcher);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Matcher)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_Matcher;
        private System.Windows.Forms.Button btn_Målrikast;
        private System.Windows.Forms.TextBox tbx_Målrikast;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_Hemmalag;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_Bortalag;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_MålHemmaLag;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_MålBortalag;
        private System.Windows.Forms.Button btn_StörstaMålskillnad;
        private System.Windows.Forms.TextBox tbx_Målskillnad;
        private System.Windows.Forms.Button btn_FlyttaRadUp;
        private System.Windows.Forms.Button btn_FlyttaRadNed;
    }
}


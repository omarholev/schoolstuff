﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Övning_3._7
{
    public partial class Form1 : Form
    {
        private List<Match> matcher = new List<Match>();

        public Form1()
        {
            InitializeComponent();
        }

        private void dgv_Matcher_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            Match nyMatch = new Match();
            matcher.Add(nyMatch);
        }

        private void dgv_Matcher_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;

            if (e.ColumnIndex == 0)
                matcher[i].HemmaLag = (string)dgv_Matcher.Rows[i].Cells[0].Value;
            else if (e.ColumnIndex == 1)
                matcher[i].BortaLag = (string)dgv_Matcher.Rows[i].Cells[1].Value;
            else if (e.ColumnIndex == 2)
                matcher[i].MålHemmaLag = int.Parse((string)dgv_Matcher.Rows[i].Cells[2].Value ); 
            else if (e.ColumnIndex == 3)
                matcher[i].MålBortaLag = int.Parse((string) dgv_Matcher.Rows[i].Cells[3].Value );

        }

        private void btn_Målrikast_Click(object sender, EventArgs e)
        {
            string hemmalag = "";
            string bortalag = "";
            int antalMål = 0;
            for (int i = 0; i < matcher.Count; i++)
            {
                if (matcher[i].MålHemmaLag + matcher[i].MålBortaLag > antalMål)
                {
                    hemmalag = matcher[i].HemmaLag;
                    bortalag = matcher[i].BortaLag;
                    antalMål = matcher[i].MålHemmaLag + matcher[i].MålBortaLag;
                }
            }
            tbx_Målrikast.Text = hemmalag + " " + bortalag + ": " + antalMål;
        }

        public class Match
        {
            public string HemmaLag { get; set; }
            public string BortaLag { get; set; }
            public int MålHemmaLag { get; set; }
            public int MålBortaLag { get; set; }

            public override string ToString()
            {
                return HemmaLag + " v.s " + BortaLag + " (" + MålHemmaLag + ", " + MålBortaLag + ")";
            }
        }

        private void dgv_Matcher_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            for (int i = 0; i < dgv_Matcher.Rows.Count; i++)
            {
                if (e.Row == dgv_Matcher.Rows[i])
                {
                    matcher[i].ToString();
                    matcher.RemoveAt(i);
                    break;
                }
            }
        }

        private void btn_StörstaMålskillnad_Click(object sender, EventArgs e)
        {
            Match match = new Match();
            int störstMålskillnad = -1;

            foreach (Match m in matcher)
            {
                if (Math.Abs(m.MålBortaLag - m.MålHemmaLag) > störstMålskillnad)
                {
                    störstMålskillnad = Math.Abs(m.MålBortaLag - m.MålHemmaLag);
                    match = m;
                }
            }

            tbx_Målskillnad.Text = match.ToString() + " " + störstMålskillnad;
        }

        private void btn_FlyttaRadUp_Click(object sender, EventArgs e)
        {
            List<Match> temp = new List<Match>();
            DataGridViewRow tempRows = dgv_Matcher.SelectedRows[0];
            int startIndex = dgv_Matcher.SelectedRows[0].Index;
            

            temp = matcher.GetRange(startIndex, dgv_Matcher.SelectedRows.Count);
            matcher.RemoveRange(startIndex, dgv_Matcher.SelectedRows.Count);
            matcher.InsertRange(startIndex - 1, temp);

            dgv_Matcher.Rows.RemoveAt(startIndex);
            dgv_Matcher.Rows.InsertRange(startIndex - 1, tempRows);

        }

        private void btn_FlyttaRadNed_Click(object sender, EventArgs e)
        {
            List<Match> temp = new List<Match>();
            DataGridViewRow tempRows = dgv_Matcher.SelectedRows[0];
            int startIndex = dgv_Matcher.SelectedRows[0].Index;


            temp = matcher.GetRange(startIndex, dgv_Matcher.SelectedRows.Count);
            matcher.RemoveRange(startIndex, dgv_Matcher.SelectedRows.Count);
            matcher.InsertRange(startIndex + 1, temp);

            dgv_Matcher.Rows.RemoveAt(startIndex);
            dgv_Matcher.Rows.InsertRange(startIndex + 1, tempRows);
        }
    }
}

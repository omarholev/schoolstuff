﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Övning_3._6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string selection;
        private void btn_Font_Click(object sender, EventArgs e)
        {
            DialogResult r = fontDialog.ShowDialog();
            if ( r == DialogResult.OK)
            {
                rtf_Text.SelectionFont = fontDialog.Font;
            }
        }



        private void lbl_ColorDialogueTrigger_Click(object sender, EventArgs e)
        {
            DialogResult r = colorDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                rtf_Text.SelectionColor = colorDialog.Color;
            }
        }

        private void rbn_LeftAlignment_Click(object sender, EventArgs e)
        {
            rtf_Text.SelectionAlignment = HorizontalAlignment.Left;
        }

        private void rbn_RightAlignment_Click(object sender, EventArgs e)
        {
            rtf_Text.SelectionAlignment = HorizontalAlignment.Right;
        }

        private void rbn_CenterAlignment_Click(object sender, EventArgs e)
        {
            rtf_Text.SelectionAlignment = HorizontalAlignment.Center;
        }
    }
}

﻿namespace Övning_3._6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.rtf_Text = new System.Windows.Forms.RichTextBox();
            this.btn_Font = new System.Windows.Forms.Button();
            this.rbn_LeftAlignment = new System.Windows.Forms.RadioButton();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.lbl_ColorDialogueTrigger = new System.Windows.Forms.Label();
            this.rbn_CenterAlignment = new System.Windows.Forms.RadioButton();
            this.rbn_RightAlignment = new System.Windows.Forms.RadioButton();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.SuspendLayout();
            // 
            // rtf_Text
            // 
            this.rtf_Text.Location = new System.Drawing.Point(13, 53);
            this.rtf_Text.Name = "rtf_Text";
            this.rtf_Text.Size = new System.Drawing.Size(635, 337);
            this.rtf_Text.TabIndex = 0;
            this.rtf_Text.Text = "";
            // 
            // btn_Font
            // 
            this.btn_Font.Location = new System.Drawing.Point(13, 13);
            this.btn_Font.Name = "btn_Font";
            this.btn_Font.Size = new System.Drawing.Size(85, 23);
            this.btn_Font.TabIndex = 1;
            this.btn_Font.Text = "Font";
            this.btn_Font.UseVisualStyleBackColor = true;
            this.btn_Font.Click += new System.EventHandler(this.btn_Font_Click);
            // 
            // rbn_LeftAlignment
            // 
            this.rbn_LeftAlignment.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbn_LeftAlignment.AutoSize = true;
            this.rbn_LeftAlignment.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rbn_LeftAlignment.BackgroundImage")));
            this.rbn_LeftAlignment.Checked = true;
            this.rbn_LeftAlignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.rbn_LeftAlignment.Location = new System.Drawing.Point(116, 13);
            this.rbn_LeftAlignment.MaximumSize = new System.Drawing.Size(25, 25);
            this.rbn_LeftAlignment.MinimumSize = new System.Drawing.Size(25, 25);
            this.rbn_LeftAlignment.Name = "rbn_LeftAlignment";
            this.rbn_LeftAlignment.Size = new System.Drawing.Size(25, 25);
            this.rbn_LeftAlignment.TabIndex = 2;
            this.rbn_LeftAlignment.TabStop = true;
            this.rbn_LeftAlignment.UseVisualStyleBackColor = true;
            this.rbn_LeftAlignment.Click += new System.EventHandler(this.rbn_LeftAlignment_Click);
            // 
            // lbl_ColorDialogueTrigger
            // 
            this.lbl_ColorDialogueTrigger.AutoSize = true;
            this.lbl_ColorDialogueTrigger.BackColor = System.Drawing.Color.Black;
            this.lbl_ColorDialogueTrigger.Location = new System.Drawing.Point(226, 18);
            this.lbl_ColorDialogueTrigger.Name = "lbl_ColorDialogueTrigger";
            this.lbl_ColorDialogueTrigger.Size = new System.Drawing.Size(13, 13);
            this.lbl_ColorDialogueTrigger.TabIndex = 5;
            this.lbl_ColorDialogueTrigger.Text = "  ";
            this.lbl_ColorDialogueTrigger.Click += new System.EventHandler(this.lbl_ColorDialogueTrigger_Click);
            // 
            // rbn_CenterAlignment
            // 
            this.rbn_CenterAlignment.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbn_CenterAlignment.AutoSize = true;
            this.rbn_CenterAlignment.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rbn_CenterAlignment.BackgroundImage")));
            this.rbn_CenterAlignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.rbn_CenterAlignment.Location = new System.Drawing.Point(147, 13);
            this.rbn_CenterAlignment.MaximumSize = new System.Drawing.Size(25, 25);
            this.rbn_CenterAlignment.MinimumSize = new System.Drawing.Size(25, 25);
            this.rbn_CenterAlignment.Name = "rbn_CenterAlignment";
            this.rbn_CenterAlignment.Size = new System.Drawing.Size(25, 25);
            this.rbn_CenterAlignment.TabIndex = 3;
            this.rbn_CenterAlignment.UseVisualStyleBackColor = true;
            this.rbn_CenterAlignment.Click += new System.EventHandler(this.rbn_CenterAlignment_Click);
            // 
            // rbn_RightAlignment
            // 
            this.rbn_RightAlignment.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbn_RightAlignment.AutoSize = true;
            this.rbn_RightAlignment.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rbn_RightAlignment.BackgroundImage")));
            this.rbn_RightAlignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.rbn_RightAlignment.Location = new System.Drawing.Point(178, 13);
            this.rbn_RightAlignment.MaximumSize = new System.Drawing.Size(25, 25);
            this.rbn_RightAlignment.MinimumSize = new System.Drawing.Size(25, 25);
            this.rbn_RightAlignment.Name = "rbn_RightAlignment";
            this.rbn_RightAlignment.Size = new System.Drawing.Size(25, 25);
            this.rbn_RightAlignment.TabIndex = 4;
            this.rbn_RightAlignment.UseVisualStyleBackColor = true;
            this.rbn_RightAlignment.Click += new System.EventHandler(this.rbn_RightAlignment_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 420);
            this.Controls.Add(this.rbn_RightAlignment);
            this.Controls.Add(this.rbn_CenterAlignment);
            this.Controls.Add(this.lbl_ColorDialogueTrigger);
            this.Controls.Add(this.rbn_LeftAlignment);
            this.Controls.Add(this.btn_Font);
            this.Controls.Add(this.rtf_Text);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtf_Text;
        private System.Windows.Forms.Button btn_Font;
        private System.Windows.Forms.RadioButton rbn_LeftAlignment;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Label lbl_ColorDialogueTrigger;
        private System.Windows.Forms.RadioButton rbn_CenterAlignment;
        private System.Windows.Forms.RadioButton rbn_RightAlignment;
        private System.Windows.Forms.FontDialog fontDialog;
    }
}


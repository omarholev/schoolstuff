﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Övning_2._1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Media> media = new List<Media>();

        private void btn_Bok_Click(object sender, EventArgs e)
        {
            Book book = new Book(tbx_BookTitle.Text, int.Parse(tbx_BookNumPages.Text));
            media.Add(book);
            lbx_Media.Items.Add(book);
        }

        private void btn_Ljudspår_Click(object sender, EventArgs e)
        {
            Sound sound = new Sound(tbx_SoundTitle.Text, int.Parse(tbx_SoundPlaytime.Text));
            media.Add(sound);
            lbx_Media.Items.Add(sound);
        }

        private void btn_Film_Click(object sender, EventArgs e)
        {
            Film film = new Film(tbx_FilmTitle.Text, int.Parse(tbx_FilmPlaytime.Text), tbx_FilmResolution.Text);
            media.Add(film);
            lbx_Media.Items.Add(film);
        }
    }
}

﻿namespace Övning_2._1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbx_Bok = new System.Windows.Forms.GroupBox();
            this.gbx_Ljudspår = new System.Windows.Forms.GroupBox();
            this.gbx_Film = new System.Windows.Forms.GroupBox();
            this.tbx_BookTitle = new System.Windows.Forms.TextBox();
            this.tbx_BookNumPages = new System.Windows.Forms.TextBox();
            this.tbx_SoundPlaytime = new System.Windows.Forms.TextBox();
            this.tbx_SoundTitle = new System.Windows.Forms.TextBox();
            this.tbx_FilmPlaytime = new System.Windows.Forms.TextBox();
            this.tbx_FilmTitle = new System.Windows.Forms.TextBox();
            this.tbx_FilmResolution = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_Bok = new System.Windows.Forms.Button();
            this.btn_Ljudspår = new System.Windows.Forms.Button();
            this.btn_Film = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.lbx_Media = new System.Windows.Forms.ListBox();
            this.gbx_Bok.SuspendLayout();
            this.gbx_Ljudspår.SuspendLayout();
            this.gbx_Film.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbx_Bok
            // 
            this.gbx_Bok.Controls.Add(this.btn_Bok);
            this.gbx_Bok.Controls.Add(this.label4);
            this.gbx_Bok.Controls.Add(this.label1);
            this.gbx_Bok.Controls.Add(this.tbx_BookNumPages);
            this.gbx_Bok.Controls.Add(this.tbx_BookTitle);
            this.gbx_Bok.Location = new System.Drawing.Point(13, 13);
            this.gbx_Bok.Name = "gbx_Bok";
            this.gbx_Bok.Size = new System.Drawing.Size(182, 130);
            this.gbx_Bok.TabIndex = 0;
            this.gbx_Bok.TabStop = false;
            this.gbx_Bok.Text = "Bok";
            // 
            // gbx_Ljudspår
            // 
            this.gbx_Ljudspår.Controls.Add(this.btn_Ljudspår);
            this.gbx_Ljudspår.Controls.Add(this.label5);
            this.gbx_Ljudspår.Controls.Add(this.label2);
            this.gbx_Ljudspår.Controls.Add(this.tbx_SoundPlaytime);
            this.gbx_Ljudspår.Controls.Add(this.tbx_SoundTitle);
            this.gbx_Ljudspår.Location = new System.Drawing.Point(201, 13);
            this.gbx_Ljudspår.Name = "gbx_Ljudspår";
            this.gbx_Ljudspår.Size = new System.Drawing.Size(182, 130);
            this.gbx_Ljudspår.TabIndex = 1;
            this.gbx_Ljudspår.TabStop = false;
            this.gbx_Ljudspår.Text = "Ljudspår";
            // 
            // gbx_Film
            // 
            this.gbx_Film.Controls.Add(this.label7);
            this.gbx_Film.Controls.Add(this.label6);
            this.gbx_Film.Controls.Add(this.label3);
            this.gbx_Film.Controls.Add(this.tbx_FilmResolution);
            this.gbx_Film.Controls.Add(this.tbx_FilmPlaytime);
            this.gbx_Film.Controls.Add(this.tbx_FilmTitle);
            this.gbx_Film.Location = new System.Drawing.Point(389, 13);
            this.gbx_Film.Name = "gbx_Film";
            this.gbx_Film.Size = new System.Drawing.Size(182, 130);
            this.gbx_Film.TabIndex = 1;
            this.gbx_Film.TabStop = false;
            this.gbx_Film.Text = "Film";
            // 
            // tbx_BookTitle
            // 
            this.tbx_BookTitle.Location = new System.Drawing.Point(76, 19);
            this.tbx_BookTitle.Name = "tbx_BookTitle";
            this.tbx_BookTitle.Size = new System.Drawing.Size(100, 20);
            this.tbx_BookTitle.TabIndex = 0;
            // 
            // tbx_BookNumPages
            // 
            this.tbx_BookNumPages.Location = new System.Drawing.Point(76, 45);
            this.tbx_BookNumPages.Name = "tbx_BookNumPages";
            this.tbx_BookNumPages.Size = new System.Drawing.Size(100, 20);
            this.tbx_BookNumPages.TabIndex = 1;
            // 
            // tbx_SoundPlaytime
            // 
            this.tbx_SoundPlaytime.Location = new System.Drawing.Point(76, 45);
            this.tbx_SoundPlaytime.Name = "tbx_SoundPlaytime";
            this.tbx_SoundPlaytime.Size = new System.Drawing.Size(100, 20);
            this.tbx_SoundPlaytime.TabIndex = 3;
            // 
            // tbx_SoundTitle
            // 
            this.tbx_SoundTitle.Location = new System.Drawing.Point(76, 19);
            this.tbx_SoundTitle.Name = "tbx_SoundTitle";
            this.tbx_SoundTitle.Size = new System.Drawing.Size(100, 20);
            this.tbx_SoundTitle.TabIndex = 2;
            // 
            // tbx_FilmPlaytime
            // 
            this.tbx_FilmPlaytime.Location = new System.Drawing.Point(76, 45);
            this.tbx_FilmPlaytime.Name = "tbx_FilmPlaytime";
            this.tbx_FilmPlaytime.Size = new System.Drawing.Size(100, 20);
            this.tbx_FilmPlaytime.TabIndex = 5;
            // 
            // tbx_FilmTitle
            // 
            this.tbx_FilmTitle.Location = new System.Drawing.Point(76, 19);
            this.tbx_FilmTitle.Name = "tbx_FilmTitle";
            this.tbx_FilmTitle.Size = new System.Drawing.Size(100, 20);
            this.tbx_FilmTitle.TabIndex = 4;
            // 
            // tbx_FilmResolution
            // 
            this.tbx_FilmResolution.Location = new System.Drawing.Point(76, 71);
            this.tbx_FilmResolution.Name = "tbx_FilmResolution";
            this.tbx_FilmResolution.Size = new System.Drawing.Size(100, 20);
            this.tbx_FilmResolution.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Titel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Titel";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Titel";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Antal Sidor";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Speltid";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Speltid";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Upplösning";
            // 
            // btn_Bok
            // 
            this.btn_Bok.Location = new System.Drawing.Point(10, 97);
            this.btn_Bok.Name = "btn_Bok";
            this.btn_Bok.Size = new System.Drawing.Size(166, 23);
            this.btn_Bok.TabIndex = 4;
            this.btn_Bok.Text = "Registrera Bok";
            this.btn_Bok.UseVisualStyleBackColor = true;
            this.btn_Bok.Click += new System.EventHandler(this.btn_Bok_Click);
            // 
            // btn_Ljudspår
            // 
            this.btn_Ljudspår.Location = new System.Drawing.Point(10, 97);
            this.btn_Ljudspår.Name = "btn_Ljudspår";
            this.btn_Ljudspår.Size = new System.Drawing.Size(166, 23);
            this.btn_Ljudspår.TabIndex = 5;
            this.btn_Ljudspår.Text = "Registrera Ljudspår";
            this.btn_Ljudspår.UseVisualStyleBackColor = true;
            this.btn_Ljudspår.Click += new System.EventHandler(this.btn_Ljudspår_Click);
            // 
            // btn_Film
            // 
            this.btn_Film.Location = new System.Drawing.Point(399, 110);
            this.btn_Film.Name = "btn_Film";
            this.btn_Film.Size = new System.Drawing.Size(166, 23);
            this.btn_Film.TabIndex = 6;
            this.btn_Film.Text = "Registrera Film";
            this.btn_Film.UseVisualStyleBackColor = true;
            this.btn_Film.Click += new System.EventHandler(this.btn_Film_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(172, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(211, 31);
            this.label8.TabIndex = 7;
            this.label8.Text = "B I B L I O T E K";
            // 
            // lbx_Media
            // 
            this.lbx_Media.FormattingEnabled = true;
            this.lbx_Media.Location = new System.Drawing.Point(13, 179);
            this.lbx_Media.Name = "lbx_Media";
            this.lbx_Media.Size = new System.Drawing.Size(552, 238);
            this.lbx_Media.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 431);
            this.Controls.Add(this.lbx_Media);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btn_Film);
            this.Controls.Add(this.gbx_Ljudspår);
            this.Controls.Add(this.gbx_Film);
            this.Controls.Add(this.gbx_Bok);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbx_Bok.ResumeLayout(false);
            this.gbx_Bok.PerformLayout();
            this.gbx_Ljudspår.ResumeLayout(false);
            this.gbx_Ljudspår.PerformLayout();
            this.gbx_Film.ResumeLayout(false);
            this.gbx_Film.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbx_Bok;
        private System.Windows.Forms.Button btn_Bok;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_BookNumPages;
        private System.Windows.Forms.TextBox tbx_BookTitle;
        private System.Windows.Forms.GroupBox gbx_Ljudspår;
        private System.Windows.Forms.Button btn_Ljudspår;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbx_SoundPlaytime;
        private System.Windows.Forms.TextBox tbx_SoundTitle;
        private System.Windows.Forms.GroupBox gbx_Film;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbx_FilmResolution;
        private System.Windows.Forms.TextBox tbx_FilmPlaytime;
        private System.Windows.Forms.TextBox tbx_FilmTitle;
        private System.Windows.Forms.Button btn_Film;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox lbx_Media;
    }
}


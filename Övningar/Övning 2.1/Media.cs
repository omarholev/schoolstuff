﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Övning_2._1
{
    class Media
    {
        protected string title;
        
        public Media (string title)
        {
            this.title = title;
        }
    }

    class Book : Media
    {
        protected int numPages = 0;

        public Book ( string title, int numPages) : base( title )
        {
            this.numPages = numPages;
        }
        public override string ToString()
        {
            return title + ": " + numPages + " sidor";
        }
    }

    class Sound : Media
    {
        protected int numMinutes = 0;
        public Sound ( string title, int numMinutes ) : base( title )
        {
            this.numMinutes = numMinutes;
        }

        public override string ToString()
        {
            return title + ": " + numMinutes + " minuter";
        }
    }

    class Film : Sound
    {
        protected string resolution;
        public Film(string title, int numMinutes, string resolution) : base(title, numMinutes)
        {
            this.resolution = resolution;
        }

        public override string ToString()
        {
            return title + ": " + numMinutes + " minuter";

        }
    }

}

﻿namespace Övningar
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Run = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbx_num1 = new System.Windows.Forms.TextBox();
            this.tbx_num2 = new System.Windows.Forms.TextBox();
            this.tbx_OutputSum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_Run
            // 
            this.btn_Run.Location = new System.Drawing.Point(124, 182);
            this.btn_Run.Name = "btn_Run";
            this.btn_Run.Size = new System.Drawing.Size(86, 23);
            this.btn_Run.TabIndex = 0;
            this.btn_Run.Text = "Run";
            this.btn_Run.UseVisualStyleBackColor = true;
            this.btn_Run.Click += new System.EventHandler(this.btn_Run_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(117, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tal 2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tal 1";
            // 
            // tbx_num1
            // 
            this.tbx_num1.Location = new System.Drawing.Point(16, 142);
            this.tbx_num1.Name = "tbx_num1";
            this.tbx_num1.Size = new System.Drawing.Size(78, 20);
            this.tbx_num1.TabIndex = 3;
            // 
            // tbx_num2
            // 
            this.tbx_num2.Location = new System.Drawing.Point(120, 142);
            this.tbx_num2.Name = "tbx_num2";
            this.tbx_num2.Size = new System.Drawing.Size(90, 20);
            this.tbx_num2.TabIndex = 4;
            // 
            // tbx_OutputSum
            // 
            this.tbx_OutputSum.Location = new System.Drawing.Point(16, 185);
            this.tbx_OutputSum.Name = "tbx_OutputSum";
            this.tbx_OutputSum.Size = new System.Drawing.Size(78, 20);
            this.tbx_OutputSum.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Summa";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbx_OutputSum);
            this.Controls.Add(this.tbx_num2);
            this.Controls.Add(this.tbx_num1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Run);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Run;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbx_num1;
        private System.Windows.Forms.TextBox tbx_num2;
        private System.Windows.Forms.TextBox tbx_OutputSum;
        private System.Windows.Forms.Label label3;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Övningar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Run_Click(object sender, EventArgs e)
        {
            int tal1 = ParseToInt(tbx_num1.Text);
            int tal2 = ParseToInt(tbx_num2.Text);

            int summa = tal1 + tal2;

            tbx_OutputSum.Text = summa.ToString();
        }



        private int ParseToInt( string text )
        {
            int tal = 0;

            int positionalValue = 1;

            for ( int i = text.Length - 1; i >= 0; i-- )
            {
                if ( text[i] >= 48 && text[i] < 59 )
                {
                    int teckenVärde = text[i] - 48;
                    tal += teckenVärde * positionalValue;
                    positionalValue *= 10;
                }
                else if(text[0] == '-')
                {
                    tal *= -1;
                    break;
                }
                else
                {
                    
                }
            }
            

            return tal;

        }
    }
}

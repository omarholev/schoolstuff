﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Övning_1._2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string[,] tabell2d = new string[4, 4];
        private void btn_Ändra_Click(object sender, EventArgs e)
        {
            int rad = int.Parse(tbx_Rad.Text);
            int kolumn = int.Parse(tbx_Kolumn.Text);
                       
            for (int i = 0; i < tabell2d.GetLongLength(1); i++)
            {
                for (int j = 0; j < tabell2d.GetLongLength(0); j++)
                {
                    if (i == 0)
                    {
                        tabell2d[i, j] = j.ToString() + "   ";
                    }

                    else if (j == 0)
                    {
                        tabell2d[i, j] = i.ToString() + "   ";
                    }

                    else
                    {
                        tabell2d[i, j] = "Ö  ";
                    }
                }
            }


            tabell2d[rad, kolumn] = tbx_Värde.Text + "   ";
            tbx_Output.Clear();
            writeTabel(tabell2d);

        }

        public void writeTabel(string[,] strings)
        {
            for (int i = 0; i < strings.GetLongLength(1); i++)
            {
                for (int j = 0; j < strings.GetLongLength(0); j++)
                {
                    tbx_Output.Text += strings[i, j] + "   ";
                }

                tbx_Output.Text += System.Environment.NewLine;
            }
        }
    }
}

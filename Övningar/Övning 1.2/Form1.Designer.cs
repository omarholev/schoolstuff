﻿namespace Övning_1._2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Ändra = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbx_Rad = new System.Windows.Forms.TextBox();
            this.tbx_Kolumn = new System.Windows.Forms.TextBox();
            this.tbx_Värde = new System.Windows.Forms.TextBox();
            this.tbx_Output = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(340, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Rad";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(415, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Kolumn";
            // 
            // btn_Ändra
            // 
            this.btn_Ändra.Location = new System.Drawing.Point(343, 142);
            this.btn_Ändra.Name = "btn_Ändra";
            this.btn_Ändra.Size = new System.Drawing.Size(75, 23);
            this.btn_Ändra.TabIndex = 3;
            this.btn_Ändra.Text = "Ändra";
            this.btn_Ändra.UseVisualStyleBackColor = true;
            this.btn_Ändra.Click += new System.EventHandler(this.btn_Ändra_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(340, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Värde";
            // 
            // tbx_Rad
            // 
            this.tbx_Rad.Location = new System.Drawing.Point(343, 44);
            this.tbx_Rad.Name = "tbx_Rad";
            this.tbx_Rad.Size = new System.Drawing.Size(47, 20);
            this.tbx_Rad.TabIndex = 5;
            // 
            // tbx_Kolumn
            // 
            this.tbx_Kolumn.Location = new System.Drawing.Point(418, 43);
            this.tbx_Kolumn.Name = "tbx_Kolumn";
            this.tbx_Kolumn.Size = new System.Drawing.Size(54, 20);
            this.tbx_Kolumn.TabIndex = 6;
            // 
            // tbx_Värde
            // 
            this.tbx_Värde.Location = new System.Drawing.Point(343, 95);
            this.tbx_Värde.Name = "tbx_Värde";
            this.tbx_Värde.Size = new System.Drawing.Size(54, 20);
            this.tbx_Värde.TabIndex = 7;
            // 
            // tbx_Output
            // 
            this.tbx_Output.Location = new System.Drawing.Point(12, 27);
            this.tbx_Output.Multiline = true;
            this.tbx_Output.Name = "tbx_Output";
            this.tbx_Output.Size = new System.Drawing.Size(297, 258);
            this.tbx_Output.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 314);
            this.Controls.Add(this.tbx_Output);
            this.Controls.Add(this.tbx_Värde);
            this.Controls.Add(this.tbx_Kolumn);
            this.Controls.Add(this.tbx_Rad);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_Ändra);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Ändra;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbx_Rad;
        private System.Windows.Forms.TextBox tbx_Kolumn;
        private System.Windows.Forms.TextBox tbx_Värde;
        private System.Windows.Forms.TextBox tbx_Output;
    }
}


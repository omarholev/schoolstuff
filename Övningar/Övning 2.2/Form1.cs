﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Övning_2._2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Employed> employees = new List<Employed>();
        
        private void btn_RegisterSale_Click(object sender, EventArgs e)
        {
            string name = tbx_SellerName.Text;
            double provision = double.Parse(tbx_SellerProvision.Text);
            double sale = double.Parse(tbx_SellerSale.Text);

            Seller seller = new Seller(name, provision, sale);

            double salary = seller.CalculateSalary();

            employees.Add(seller);
            lbx_Register.Items.Add(seller.ToString());
            lbx_SalaryPayments.Items.Add(seller.ToString() + ": " + salary.ToString());
        }

        private void btn_RegisterShift_Click(object sender, EventArgs e)
        {
            string name = tbx_ConsultName.Text;
            double hoursWorked = double.Parse(tbx_ConsultTimeWorked.Text);
            double hourlySalary = double.Parse(tbx_ConsultHourlySalary.Text);

            Consult consult = new Consult(name, hoursWorked, hourlySalary);

            double salary = consult.CalculateSalary();

            employees.Add(consult);
            lbx_Register.Items.Add(consult.ToString());
            lbx_SalaryPayments.Items.Add(consult.ToString() + ": " + salary.ToString());
        }

        private void btn_RegisterMonthlySalary_Click(object sender, EventArgs e)
        {
            string name = tbx_OfficeWorkerName.Text;
            double monthlySalary = double.Parse(tbx_OfficeWorkerMonthlySalary.Text);

            OfficeEmployee officeEmployee = new OfficeEmployee(name, monthlySalary);

            double salary = officeEmployee.CalculateSalary();
            employees.Add(officeEmployee);
            lbx_Register.Items.Add(officeEmployee.ToString());
            lbx_SalaryPayments.Items.Add(officeEmployee.ToString() + ": " + salary);

        }

        private void btn_CalculateSalary_Click(object sender, EventArgs e)
        {
            double totalSalaryPayment = 0;
            for (int i = 0; i < employees.Count(); i++)
            {
                totalSalaryPayment += employees[i].CalculateSalary();
            }

            tbx_TotalSalary.Text = totalSalaryPayment.ToString();
        }
    }
}

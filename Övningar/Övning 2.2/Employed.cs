﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Övning_2._2
{
    class Employed
    {
        public string name;
        public virtual double CalculateSalary()
        {
            return 0;
        }
    }

    class Seller : Employed
    {
        double provision;
        double sale;

        public Seller(string n, double p, double s)
        {
            this.name = n;
            this.provision = p;
            this.sale = s;

        }
        public override double CalculateSalary()
        {
            return sale * provision;
        }
        public override string ToString()
        {
            return name + " (Seller)";
        }

    }

    class Consult : Employed
    {
        double hoursWorked;
        double hourlySalary;

        public Consult(string n, double w, double s)
        {
            this.name = n;
            this.hoursWorked = w;
            this.hourlySalary = s;
        }
        public override double CalculateSalary()
        {
            return hoursWorked * hourlySalary;
        }
        public override string ToString()
        {
            return name + " (Consultant)";
        }

    }

    class OfficeEmployee : Employed
    {
        double monthlySalary;

        public OfficeEmployee(string n, double m)
        {
            this.name = n;
            this.monthlySalary = m;

        }
        public override double CalculateSalary()
        {
            return monthlySalary;
        }
        public override string ToString()
        {
            return name + " (Office Employee)";
        }
    }
}

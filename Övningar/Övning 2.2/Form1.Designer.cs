﻿namespace Övning_2._2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbx_SellerName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_RegisterSale = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbx_SellerProvision = new System.Windows.Forms.TextBox();
            this.tbx_SellerSale = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbx_ConsultTimeWorked = new System.Windows.Forms.TextBox();
            this.tbx_ConsultHourlySalary = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbx_ConsultName = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbx_OfficeWorkerMonthlySalary = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbx_OfficeWorkerName = new System.Windows.Forms.TextBox();
            this.lbx_Register = new System.Windows.Forms.ListBox();
            this.lbx_SalaryPayments = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbx_TotalSalary = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_RegisterShift = new System.Windows.Forms.Button();
            this.btn_RegisterMonthlySalary = new System.Windows.Forms.Button();
            this.btn_CalculateSalary = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbx_SellerName
            // 
            this.tbx_SellerName.Location = new System.Drawing.Point(62, 31);
            this.tbx_SellerName.Name = "tbx_SellerName";
            this.tbx_SellerName.Size = new System.Drawing.Size(92, 20);
            this.tbx_SellerName.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbx_SellerSale);
            this.groupBox1.Controls.Add(this.tbx_SellerProvision);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbx_SellerName);
            this.groupBox1.Controls.Add(this.btn_RegisterSale);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(160, 150);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Säljare";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Namn";
            // 
            // btn_RegisterSale
            // 
            this.btn_RegisterSale.Location = new System.Drawing.Point(9, 121);
            this.btn_RegisterSale.Name = "btn_RegisterSale";
            this.btn_RegisterSale.Size = new System.Drawing.Size(138, 23);
            this.btn_RegisterSale.TabIndex = 3;
            this.btn_RegisterSale.Text = "Registrera Försäljning";
            this.btn_RegisterSale.UseVisualStyleBackColor = true;
            this.btn_RegisterSale.Click += new System.EventHandler(this.btn_RegisterSale_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Provision";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Försäljning";
            // 
            // tbx_SellerProvision
            // 
            this.tbx_SellerProvision.Location = new System.Drawing.Point(62, 57);
            this.tbx_SellerProvision.Name = "tbx_SellerProvision";
            this.tbx_SellerProvision.Size = new System.Drawing.Size(92, 20);
            this.tbx_SellerProvision.TabIndex = 5;
            // 
            // tbx_SellerSale
            // 
            this.tbx_SellerSale.Location = new System.Drawing.Point(62, 83);
            this.tbx_SellerSale.Name = "tbx_SellerSale";
            this.tbx_SellerSale.Size = new System.Drawing.Size(92, 20);
            this.tbx_SellerSale.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_RegisterShift);
            this.groupBox2.Controls.Add(this.tbx_ConsultTimeWorked);
            this.groupBox2.Controls.Add(this.tbx_ConsultHourlySalary);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.tbx_ConsultName);
            this.groupBox2.Location = new System.Drawing.Point(178, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(160, 150);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Konsult";
            // 
            // tbx_ConsultTimeWorked
            // 
            this.tbx_ConsultTimeWorked.Location = new System.Drawing.Point(64, 83);
            this.tbx_ConsultTimeWorked.Name = "tbx_ConsultTimeWorked";
            this.tbx_ConsultTimeWorked.Size = new System.Drawing.Size(83, 20);
            this.tbx_ConsultTimeWorked.TabIndex = 6;
            // 
            // tbx_ConsultHourlySalary
            // 
            this.tbx_ConsultHourlySalary.Location = new System.Drawing.Point(64, 57);
            this.tbx_ConsultHourlySalary.Name = "tbx_ConsultHourlySalary";
            this.tbx_ConsultHourlySalary.Size = new System.Drawing.Size(83, 20);
            this.tbx_ConsultHourlySalary.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Arbetad tid";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Timlön";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Namn";
            // 
            // tbx_ConsultName
            // 
            this.tbx_ConsultName.Location = new System.Drawing.Point(64, 31);
            this.tbx_ConsultName.Name = "tbx_ConsultName";
            this.tbx_ConsultName.Size = new System.Drawing.Size(83, 20);
            this.tbx_ConsultName.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_RegisterMonthlySalary);
            this.groupBox3.Controls.Add(this.tbx_OfficeWorkerMonthlySalary);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.tbx_OfficeWorkerName);
            this.groupBox3.Location = new System.Drawing.Point(344, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(154, 150);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kontorist";
            // 
            // tbx_OfficeWorkerMonthlySalary
            // 
            this.tbx_OfficeWorkerMonthlySalary.Location = new System.Drawing.Point(71, 57);
            this.tbx_OfficeWorkerMonthlySalary.Name = "tbx_OfficeWorkerMonthlySalary";
            this.tbx_OfficeWorkerMonthlySalary.Size = new System.Drawing.Size(76, 20);
            this.tbx_OfficeWorkerMonthlySalary.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Månadslön";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Namn";
            // 
            // tbx_OfficeWorkerName
            // 
            this.tbx_OfficeWorkerName.Location = new System.Drawing.Point(71, 31);
            this.tbx_OfficeWorkerName.Name = "tbx_OfficeWorkerName";
            this.tbx_OfficeWorkerName.Size = new System.Drawing.Size(76, 20);
            this.tbx_OfficeWorkerName.TabIndex = 0;
            // 
            // lbx_Register
            // 
            this.lbx_Register.FormattingEnabled = true;
            this.lbx_Register.Location = new System.Drawing.Point(12, 214);
            this.lbx_Register.Name = "lbx_Register";
            this.lbx_Register.Size = new System.Drawing.Size(147, 147);
            this.lbx_Register.TabIndex = 9;
            // 
            // lbx_SalaryPayments
            // 
            this.lbx_SalaryPayments.FormattingEnabled = true;
            this.lbx_SalaryPayments.Location = new System.Drawing.Point(191, 214);
            this.lbx_SalaryPayments.Name = "lbx_SalaryPayments";
            this.lbx_SalaryPayments.Size = new System.Drawing.Size(147, 147);
            this.lbx_SalaryPayments.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "REGISTER";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(188, 198);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(127, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "LÖNEUTBETALNINGAR";
            // 
            // tbx_TotalSalary
            // 
            this.tbx_TotalSalary.Location = new System.Drawing.Point(358, 275);
            this.tbx_TotalSalary.Name = "tbx_TotalSalary";
            this.tbx_TotalSalary.Size = new System.Drawing.Size(133, 20);
            this.tbx_TotalSalary.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(355, 259);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Total Lönekostnad:";
            // 
            // btn_RegisterShift
            // 
            this.btn_RegisterShift.Location = new System.Drawing.Point(9, 121);
            this.btn_RegisterShift.Name = "btn_RegisterShift";
            this.btn_RegisterShift.Size = new System.Drawing.Size(138, 23);
            this.btn_RegisterShift.TabIndex = 13;
            this.btn_RegisterShift.Text = "Registrera arbetspass";
            this.btn_RegisterShift.UseVisualStyleBackColor = true;
            this.btn_RegisterShift.Click += new System.EventHandler(this.btn_RegisterShift_Click);
            // 
            // btn_RegisterMonthlySalary
            // 
            this.btn_RegisterMonthlySalary.Location = new System.Drawing.Point(9, 121);
            this.btn_RegisterMonthlySalary.Name = "btn_RegisterMonthlySalary";
            this.btn_RegisterMonthlySalary.Size = new System.Drawing.Size(139, 23);
            this.btn_RegisterMonthlySalary.TabIndex = 14;
            this.btn_RegisterMonthlySalary.Text = "Registrera månadslön";
            this.btn_RegisterMonthlySalary.UseVisualStyleBackColor = true;
            this.btn_RegisterMonthlySalary.Click += new System.EventHandler(this.btn_RegisterMonthlySalary_Click);
            // 
            // btn_CalculateSalary
            // 
            this.btn_CalculateSalary.Location = new System.Drawing.Point(358, 214);
            this.btn_CalculateSalary.Name = "btn_CalculateSalary";
            this.btn_CalculateSalary.Size = new System.Drawing.Size(139, 23);
            this.btn_CalculateSalary.TabIndex = 15;
            this.btn_CalculateSalary.Text = "Beräkna Löner";
            this.btn_CalculateSalary.UseVisualStyleBackColor = true;
            this.btn_CalculateSalary.Click += new System.EventHandler(this.btn_CalculateSalary_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 397);
            this.Controls.Add(this.btn_CalculateSalary);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbx_TotalSalary);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbx_SalaryPayments);
            this.Controls.Add(this.lbx_Register);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbx_SellerName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbx_SellerSale;
        private System.Windows.Forms.TextBox tbx_SellerProvision;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_RegisterSale;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_RegisterShift;
        private System.Windows.Forms.TextBox tbx_ConsultTimeWorked;
        private System.Windows.Forms.TextBox tbx_ConsultHourlySalary;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbx_ConsultName;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_RegisterMonthlySalary;
        private System.Windows.Forms.TextBox tbx_OfficeWorkerMonthlySalary;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbx_OfficeWorkerName;
        private System.Windows.Forms.ListBox lbx_Register;
        private System.Windows.Forms.ListBox lbx_SalaryPayments;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbx_TotalSalary;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_CalculateSalary;
    }
}


﻿namespace Övning_3._5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabCtrl = new System.Windows.Forms.TabControl();
            this.tabPage_Musik = new System.Windows.Forms.TabPage();
            this.tabPage_Film = new System.Windows.Forms.TabPage();
            this.tabCtrl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCtrl
            // 
            this.tabCtrl.Controls.Add(this.tabPage_Musik);
            this.tabCtrl.Controls.Add(this.tabPage_Film);
            this.tabCtrl.Location = new System.Drawing.Point(13, 13);
            this.tabCtrl.Multiline = true;
            this.tabCtrl.Name = "tabCtrl";
            this.tabCtrl.SelectedIndex = 0;
            this.tabCtrl.Size = new System.Drawing.Size(470, 367);
            this.tabCtrl.TabIndex = 0;
            // 
            // tabPage_Musik
            // 
            this.tabPage_Musik.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Musik.Name = "tabPage_Musik";
            this.tabPage_Musik.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Musik.Size = new System.Drawing.Size(462, 341);
            this.tabPage_Musik.TabIndex = 0;
            this.tabPage_Musik.Text = "Musik";
            this.tabPage_Musik.UseVisualStyleBackColor = true;
            // 
            // tabPage_Film
            // 
            this.tabPage_Film.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Film.Name = "tabPage_Film";
            this.tabPage_Film.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Film.Size = new System.Drawing.Size(462, 341);
            this.tabPage_Film.TabIndex = 1;
            this.tabPage_Film.Text = "Film";
            this.tabPage_Film.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabCtrl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabCtrl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCtrl;
        private System.Windows.Forms.TabPage tabPage_Musik;
        private System.Windows.Forms.TabPage tabPage_Film;
    }
}


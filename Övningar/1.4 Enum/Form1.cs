﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1._4_Enum
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Preview_Click(object sender, EventArgs e)
        {
            FontStyle style = FontStyle.Regular;

            if (cbx_Bold.Checked) style = style | FontStyle.Bold;
            if (cbx_Italic.Checked) style = style | FontStyle.Italic;
            if (cbx_Underlined.Checked) style = style | FontStyle.Underline;
            if (cbx_Strikeout.Checked) style = style | FontStyle.Strikeout;

            int fontSize = int.Parse(num_Size.Text);
            string fontStyle = tbx_Font.Text;

            Font font = new Font(fontStyle, fontSize, style);
            tbx_Preview.Font = font;
        }
    }
}

﻿namespace _1._4_Enum
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbx_Preview = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbx_Font = new System.Windows.Forms.TextBox();
            this.cbx_Bold = new System.Windows.Forms.CheckBox();
            this.cbx_Italic = new System.Windows.Forms.CheckBox();
            this.num_Size = new System.Windows.Forms.NumericUpDown();
            this.cbx_Underlined = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_Preview = new System.Windows.Forms.Button();
            this.cbx_Strikeout = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.num_Size)).BeginInit();
            this.SuspendLayout();
            // 
            // tbx_Preview
            // 
            this.tbx_Preview.Location = new System.Drawing.Point(12, 33);
            this.tbx_Preview.Multiline = true;
            this.tbx_Preview.Name = "tbx_Preview";
            this.tbx_Preview.Size = new System.Drawing.Size(259, 190);
            this.tbx_Preview.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 230);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Font";
            // 
            // tbx_Font
            // 
            this.tbx_Font.Location = new System.Drawing.Point(15, 247);
            this.tbx_Font.Name = "tbx_Font";
            this.tbx_Font.Size = new System.Drawing.Size(100, 20);
            this.tbx_Font.TabIndex = 2;
            // 
            // cbx_Bold
            // 
            this.cbx_Bold.AutoSize = true;
            this.cbx_Bold.Location = new System.Drawing.Point(15, 274);
            this.cbx_Bold.Name = "cbx_Bold";
            this.cbx_Bold.Size = new System.Drawing.Size(47, 17);
            this.cbx_Bold.TabIndex = 3;
            this.cbx_Bold.Text = "Bold";
            this.cbx_Bold.UseVisualStyleBackColor = true;
            // 
            // cbx_Italic
            // 
            this.cbx_Italic.AutoSize = true;
            this.cbx_Italic.Location = new System.Drawing.Point(15, 297);
            this.cbx_Italic.Name = "cbx_Italic";
            this.cbx_Italic.Size = new System.Drawing.Size(48, 17);
            this.cbx_Italic.TabIndex = 4;
            this.cbx_Italic.Text = "Italic";
            this.cbx_Italic.UseVisualStyleBackColor = true;
            // 
            // num_Size
            // 
            this.num_Size.Location = new System.Drawing.Point(121, 248);
            this.num_Size.Name = "num_Size";
            this.num_Size.Size = new System.Drawing.Size(50, 20);
            this.num_Size.TabIndex = 6;
            // 
            // cbx_Underlined
            // 
            this.cbx_Underlined.AutoSize = true;
            this.cbx_Underlined.Location = new System.Drawing.Point(15, 320);
            this.cbx_Underlined.Name = "cbx_Underlined";
            this.cbx_Underlined.Size = new System.Drawing.Size(77, 17);
            this.cbx_Underlined.TabIndex = 7;
            this.cbx_Underlined.Text = "Underlined";
            this.cbx_Underlined.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Preview";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(121, 232);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Size";
            // 
            // btn_Preview
            // 
            this.btn_Preview.Location = new System.Drawing.Point(196, 339);
            this.btn_Preview.Name = "btn_Preview";
            this.btn_Preview.Size = new System.Drawing.Size(75, 23);
            this.btn_Preview.TabIndex = 10;
            this.btn_Preview.Text = "Preview";
            this.btn_Preview.UseVisualStyleBackColor = true;
            this.btn_Preview.Click += new System.EventHandler(this.btn_Preview_Click);
            // 
            // cbx_Strikeout
            // 
            this.cbx_Strikeout.AutoSize = true;
            this.cbx_Strikeout.Location = new System.Drawing.Point(15, 343);
            this.cbx_Strikeout.Name = "cbx_Strikeout";
            this.cbx_Strikeout.Size = new System.Drawing.Size(68, 17);
            this.cbx_Strikeout.TabIndex = 11;
            this.cbx_Strikeout.Text = "Strikeout";
            this.cbx_Strikeout.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 369);
            this.Controls.Add(this.cbx_Strikeout);
            this.Controls.Add(this.btn_Preview);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbx_Underlined);
            this.Controls.Add(this.num_Size);
            this.Controls.Add(this.cbx_Italic);
            this.Controls.Add(this.cbx_Bold);
            this.Controls.Add(this.tbx_Font);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbx_Preview);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.num_Size)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbx_Preview;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_Font;
        private System.Windows.Forms.CheckBox cbx_Bold;
        private System.Windows.Forms.CheckBox cbx_Italic;
        private System.Windows.Forms.NumericUpDown num_Size;
        private System.Windows.Forms.CheckBox cbx_Underlined;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_Preview;
        private System.Windows.Forms.CheckBox cbx_Strikeout;
    }
}


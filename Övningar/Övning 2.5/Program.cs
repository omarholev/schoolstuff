﻿using System;

namespace Övning_2._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Saker<int> saker = new Saker<int>();
            for (int i = 0; i < 30; i++) saker.LäggTill(i);
            for (int i = 0; i < saker.Antal(); i++) Console.WriteLine(saker.ElementFrån(i));

            Console.WriteLine("Har siffran 5. " + saker.Har(5));
            Console.WriteLine("Har siffran 31. " + saker.Har(31));

            Console.WriteLine("Var är siffran 20?" + saker.Sök(20));
            Console.WriteLine("Var är siffran 31?" + saker.Sök(31));

            Console.ReadLine();
        }
    }

    class Saker<T>
    {
        protected int buffert;
        protected T[] lista;
        protected int längd;
        protected int antal;

        public Saker ( )
        {
            buffert = 30;
            antal = 0;
            längd = 30;
            lista = new T[längd];
        }

        protected void Expandera ( int storlek )
        {
            if (storlek < 1) return;
            T[] temp = new T[längd + storlek];

            for (int i = 0; i < antal; i++) temp[i] = lista[i];

            lista = temp;
            längd += storlek;
        }

        protected void Reducera ()
        {
            T[] temp = new T[antal];

            for (int i = 0; i < antal; i++) temp[i] = lista[i];
            lista = temp;
            längd = antal;
        }

        public void LäggTill(T e)
        {
            if (antal + 1 > längd) Expandera(1 + buffert);

            lista[antal++] = e;
        }

        public T TaBort ( int index )
        {
            T temp = lista[index];
            
            for ( int i=index; i<antal-1; i++)
            {
                lista[i] = lista[i + 1];
            }

            antal--;

            if (längd - antal < buffert) Reducera();
            return temp;
        }

        public int Antal() { return antal; }
        public T ElementFrån( int index )
        {
            return lista[index];
        } 
        public bool Har(T värde)
        {
            bool result = false;
            for ( int i = 0; i < antal; i++)
            {
                if (lista[i].Equals(värde))
                {
                    result = true;
                }
            }
            return result;
        }
        public int Sök(T värde)
        {
            int index = -1;
            for (int i = 0; i<antal; i++)
            {
                if (lista[i].Equals(värde)) 
                { 
                    index = i; 
                    break; 
                }
            }
            return index;
        }

        
    }
}

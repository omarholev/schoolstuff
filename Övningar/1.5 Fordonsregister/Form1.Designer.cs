﻿namespace _1._5_Fordonsregister
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Registrera = new System.Windows.Forms.Button();
            this.lbx_fordon = new System.Windows.Forms.ListBox();
            this.rbn_VisaAlla = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbn_VisaMC = new System.Windows.Forms.RadioButton();
            this.rbn_VisaBilar = new System.Windows.Forms.RadioButton();
            this.tbx_RegNr = new System.Windows.Forms.TextBox();
            this.tbx_Märke = new System.Windows.Forms.TextBox();
            this.tbx_Modell = new System.Windows.Forms.TextBox();
            this.cbx_fordonsTyp = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Registrera
            // 
            this.btn_Registrera.Location = new System.Drawing.Point(82, 307);
            this.btn_Registrera.Name = "btn_Registrera";
            this.btn_Registrera.Size = new System.Drawing.Size(100, 23);
            this.btn_Registrera.TabIndex = 0;
            this.btn_Registrera.Text = "Registrera";
            this.btn_Registrera.UseVisualStyleBackColor = true;
            this.btn_Registrera.Click += new System.EventHandler(this.btn_Registrera_Click);
            // 
            // lbx_fordon
            // 
            this.lbx_fordon.FormattingEnabled = true;
            this.lbx_fordon.Location = new System.Drawing.Point(198, 39);
            this.lbx_fordon.Name = "lbx_fordon";
            this.lbx_fordon.Size = new System.Drawing.Size(221, 212);
            this.lbx_fordon.TabIndex = 1;
            // 
            // rbn_VisaAlla
            // 
            this.rbn_VisaAlla.AutoSize = true;
            this.rbn_VisaAlla.Location = new System.Drawing.Point(6, 28);
            this.rbn_VisaAlla.Name = "rbn_VisaAlla";
            this.rbn_VisaAlla.Size = new System.Drawing.Size(42, 17);
            this.rbn_VisaAlla.TabIndex = 2;
            this.rbn_VisaAlla.TabStop = true;
            this.rbn_VisaAlla.Text = "Alla";
            this.rbn_VisaAlla.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbn_VisaMC);
            this.groupBox1.Controls.Add(this.rbn_VisaBilar);
            this.groupBox1.Controls.Add(this.rbn_VisaAlla);
            this.groupBox1.Location = new System.Drawing.Point(198, 266);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(221, 64);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Visa";
            // 
            // rbn_VisaMC
            // 
            this.rbn_VisaMC.AutoSize = true;
            this.rbn_VisaMC.Location = new System.Drawing.Point(174, 28);
            this.rbn_VisaMC.Name = "rbn_VisaMC";
            this.rbn_VisaMC.Size = new System.Drawing.Size(41, 17);
            this.rbn_VisaMC.TabIndex = 4;
            this.rbn_VisaMC.TabStop = true;
            this.rbn_VisaMC.Text = "MC";
            this.rbn_VisaMC.UseVisualStyleBackColor = true;
            // 
            // rbn_VisaBilar
            // 
            this.rbn_VisaBilar.AutoSize = true;
            this.rbn_VisaBilar.Location = new System.Drawing.Point(87, 28);
            this.rbn_VisaBilar.Name = "rbn_VisaBilar";
            this.rbn_VisaBilar.Size = new System.Drawing.Size(45, 17);
            this.rbn_VisaBilar.TabIndex = 3;
            this.rbn_VisaBilar.TabStop = true;
            this.rbn_VisaBilar.Text = "Bilar";
            this.rbn_VisaBilar.UseVisualStyleBackColor = true;
            // 
            // tbx_RegNr
            // 
            this.tbx_RegNr.Location = new System.Drawing.Point(82, 150);
            this.tbx_RegNr.Name = "tbx_RegNr";
            this.tbx_RegNr.Size = new System.Drawing.Size(100, 20);
            this.tbx_RegNr.TabIndex = 4;
            // 
            // tbx_Märke
            // 
            this.tbx_Märke.Location = new System.Drawing.Point(82, 176);
            this.tbx_Märke.Name = "tbx_Märke";
            this.tbx_Märke.Size = new System.Drawing.Size(100, 20);
            this.tbx_Märke.TabIndex = 5;
            // 
            // tbx_Modell
            // 
            this.tbx_Modell.Location = new System.Drawing.Point(82, 202);
            this.tbx_Modell.Name = "tbx_Modell";
            this.tbx_Modell.Size = new System.Drawing.Size(100, 20);
            this.tbx_Modell.TabIndex = 6;
            // 
            // cbx_fordonsTyp
            // 
            this.cbx_fordonsTyp.FormattingEnabled = true;
            this.cbx_fordonsTyp.Items.AddRange(new object[] {
            "Bil",
            "MC"});
            this.cbx_fordonsTyp.Location = new System.Drawing.Point(82, 228);
            this.cbx_fordonsTyp.Name = "cbx_fordonsTyp";
            this.cbx_fordonsTyp.Size = new System.Drawing.Size(100, 21);
            this.cbx_fordonsTyp.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Reg-nr";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 179);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Märke";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 205);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Modell";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 231);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Typ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(195, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Register";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 363);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbx_fordonsTyp);
            this.Controls.Add(this.tbx_Modell);
            this.Controls.Add(this.tbx_Märke);
            this.Controls.Add(this.tbx_RegNr);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbx_fordon);
            this.Controls.Add(this.btn_Registrera);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Registrera;
        private System.Windows.Forms.ListBox lbx_fordon;
        private System.Windows.Forms.RadioButton rbn_VisaAlla;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbn_VisaMC;
        private System.Windows.Forms.RadioButton rbn_VisaBilar;
        private System.Windows.Forms.TextBox tbx_RegNr;
        private System.Windows.Forms.TextBox tbx_Märke;
        private System.Windows.Forms.TextBox tbx_Modell;
        private System.Windows.Forms.ComboBox cbx_fordonsTyp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}


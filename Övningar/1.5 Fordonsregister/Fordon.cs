﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1._5_Fordonsregister
{
    class Fordon
    {
        public enum FordonsTyp { Bil, MC }
        
        string regNr;
        string märke;
        string modell;
        FordonsTyp fordonsTyp;
        
        
        public Fordon(string regnr, string märke, string modell, FordonsTyp fordonstyp)
        {
            this.regNr = regnr;
            this.märke = märke;
            this.modell = modell;
            this.fordonsTyp = fordonstyp;
        }

        public string RegNr { get { return regNr; } }
        public string Märke { get { return märke; } }
        public string Modell { get { return modell; } }
        public string Fordonstyp { get { return fordonsTyp.ToString(); } }

        public static bool GodkännRegNr (string regnr, List<Fordon> fordon)
        {

            int correctChars = 0;
            for (int i = 0; i < regnr.Length; i++)
            {
                if (i <= 2)
                {
                    if ((int)regnr[i] > 64 && regnr[i] < 91)
                    {
                        correctChars++;
                    }
                }

                else if (i > 2 && i <= 6)
                {
                    if ((int)regnr[i] > 47 && regnr[i] < 58)
                    {
                        correctChars++;
                    }
                }
            }
            //Check for repeated regnummers
            for (int i = 0; i < fordon.Count; i++)
            {
                if (regnr == fordon[i].RegNr);
                {
                    Console.WriteLine(regnr + " | " + fordon[i].RegNr);
                    return false;
                }
            }

            if (correctChars != 6) 
            {
                Console.WriteLine("Not sufficient correct characters");
                return false;
            }
            else { return true;}
        }

        public override string ToString()
        {
            return (regNr + "    |    " + märke + "    " + modell + "    |    " + fordonsTyp.ToString());
        }


    }
}

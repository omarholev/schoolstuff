﻿namespace _3_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dlgFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.dlgColor = new System.Windows.Forms.ColorDialog();
            this.dlgFont = new System.Windows.Forms.FontDialog();
            this.btn_VäljFärg = new System.Windows.Forms.Button();
            this.btn_väljMapp = new System.Windows.Forms.Button();
            this.btn_VäljFont = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.redigeraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu_VäljFärg = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu_VäljMapp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu_VäljFont = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuCoolt = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cooltToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.VälFärgTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1.SuspendLayout();
            this.contextMenuCoolt.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_VäljFärg
            // 
            this.btn_VäljFärg.ContextMenuStrip = this.contextMenuCoolt;
            this.btn_VäljFärg.Location = new System.Drawing.Point(49, 60);
            this.btn_VäljFärg.Name = "btn_VäljFärg";
            this.btn_VäljFärg.Size = new System.Drawing.Size(97, 28);
            this.btn_VäljFärg.TabIndex = 0;
            this.btn_VäljFärg.Text = "Välj Färg";
            this.VälFärgTooltip.SetToolTip(this.btn_VäljFärg, "Detta är coolt");
            this.btn_VäljFärg.UseVisualStyleBackColor = true;
            this.btn_VäljFärg.Click += new System.EventHandler(this.btn_VäljFärg_Click);
            // 
            // btn_väljMapp
            // 
            this.btn_väljMapp.Location = new System.Drawing.Point(49, 89);
            this.btn_väljMapp.Name = "btn_väljMapp";
            this.btn_väljMapp.Size = new System.Drawing.Size(97, 28);
            this.btn_väljMapp.TabIndex = 1;
            this.btn_väljMapp.Text = "Välj Mapp";
            this.btn_väljMapp.UseVisualStyleBackColor = true;
            this.btn_väljMapp.Click += new System.EventHandler(this.btn_väljMapp_Click);
            // 
            // btn_VäljFont
            // 
            this.btn_VäljFont.Location = new System.Drawing.Point(49, 118);
            this.btn_VäljFont.Name = "btn_VäljFont";
            this.btn_VäljFont.Size = new System.Drawing.Size(97, 28);
            this.btn_VäljFont.TabIndex = 2;
            this.btn_VäljFont.Text = "Välj teckensnitt";
            this.btn_VäljFont.UseVisualStyleBackColor = true;
            this.btn_VäljFont.Click += new System.EventHandler(this.btn_VäljFont_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redigeraToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(335, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // redigeraToolStripMenuItem
            // 
            this.redigeraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnu_VäljFärg,
            this.mnu_VäljMapp,
            this.mnu_VäljFont});
            this.redigeraToolStripMenuItem.Name = "redigeraToolStripMenuItem";
            this.redigeraToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.redigeraToolStripMenuItem.Text = "Redigera";
            // 
            // mnu_VäljFärg
            // 
            this.mnu_VäljFärg.Name = "mnu_VäljFärg";
            this.mnu_VäljFärg.Size = new System.Drawing.Size(180, 22);
            this.mnu_VäljFärg.Text = "Välj Färg";
            this.mnu_VäljFärg.Click += new System.EventHandler(this.mnu_VäljFärg_Click);
            // 
            // mnu_VäljMapp
            // 
            this.mnu_VäljMapp.Name = "mnu_VäljMapp";
            this.mnu_VäljMapp.Size = new System.Drawing.Size(180, 22);
            this.mnu_VäljMapp.Text = "Välj Mapp";
            this.mnu_VäljMapp.Click += new System.EventHandler(this.mnu_VäljMapp_Click);
            // 
            // mnu_VäljFont
            // 
            this.mnu_VäljFont.Name = "mnu_VäljFont";
            this.mnu_VäljFont.Size = new System.Drawing.Size(180, 22);
            this.mnu_VäljFont.Text = "Välj Font";
            this.mnu_VäljFont.Click += new System.EventHandler(this.mnu_VäljFont_Click);
            // 
            // contextMenuCoolt
            // 
            this.contextMenuCoolt.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cooltToolStripMenuItem});
            this.contextMenuCoolt.Name = "contextMenuCoolt";
            this.contextMenuCoolt.Size = new System.Drawing.Size(104, 26);
            // 
            // cooltToolStripMenuItem
            // 
            this.cooltToolStripMenuItem.Name = "cooltToolStripMenuItem";
            this.cooltToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.cooltToolStripMenuItem.Text = "Coolt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 292);
            this.Controls.Add(this.btn_VäljFont);
            this.Controls.Add(this.btn_väljMapp);
            this.Controls.Add(this.btn_VäljFärg);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuCoolt.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog dlgFolder;
        private System.Windows.Forms.ColorDialog dlgColor;
        private System.Windows.Forms.FontDialog dlgFont;
        private System.Windows.Forms.Button btn_VäljFärg;
        private System.Windows.Forms.Button btn_väljMapp;
        private System.Windows.Forms.Button btn_VäljFont;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem redigeraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnu_VäljFärg;
        private System.Windows.Forms.ToolStripMenuItem mnu_VäljMapp;
        private System.Windows.Forms.ToolStripMenuItem mnu_VäljFont;
        private System.Windows.Forms.ContextMenuStrip contextMenuCoolt;
        private System.Windows.Forms.ToolStripMenuItem cooltToolStripMenuItem;
        private System.Windows.Forms.ToolTip VälFärgTooltip;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_VäljFärg_Click(object sender, EventArgs e)
        {
            DialogResult r = dlgColor.ShowDialog();
            if ( r == DialogResult.OK)
            {
                btn_VäljFärg.BackColor = dlgColor.Color;
            }
        }

        private void btn_väljMapp_Click(object sender, EventArgs e)
        {
            DialogResult r = dlgFolder.ShowDialog();
            if (r == DialogResult.OK)
            {
                btn_väljMapp.Text = dlgFolder.SelectedPath;
                Console.WriteLine(dlgFolder.SelectedPath);
            }
        }

        private void btn_VäljFont_Click(object sender, EventArgs e)
        {
            DialogResult r = dlgFont.ShowDialog();
            if (r == DialogResult.OK)
            {
                btn_VäljFont.Font = dlgFont.Font;
            }
        }

        private void mnu_VäljFärg_Click(object sender, EventArgs e)
        {
            DialogResult r = dlgColor.ShowDialog();
            if (r == DialogResult.OK)
            {
                btn_VäljFärg.BackColor = dlgColor.Color;
            }
        }

        private void mnu_VäljMapp_Click(object sender, EventArgs e)
        {
            DialogResult r = dlgFolder.ShowDialog();
            if (r == DialogResult.OK)
            {
                btn_väljMapp.Text = dlgFolder.SelectedPath;
                Console.WriteLine(dlgFolder.SelectedPath);
            }
        }

        private void mnu_VäljFont_Click(object sender, EventArgs e)
        {
            DialogResult r = dlgFont.ShowDialog();
            if (r == DialogResult.OK)
            {
                btn_VäljFont.Font = dlgFont.Font;
            }
        }
    }
}

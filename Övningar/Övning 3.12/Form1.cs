﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Övning_3._12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
    }
}

class LabeledTextBox : TextBox
{
    private string ledText;
    [Category("Appearance"), Description("Label text."),
        DefaultValue("...")]

    public string LedText
    {
        get { return ledText; }
        set
        {
            ledText = value; Text = value;
            ForeColor = System.Drawing.Color.Gray;
        }
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
        base.OnKeyDown(e);

        this.Clear();
        this.ForeColor = System.Drawing.Color.Black;
    }

    protected override void OnKeyUp(KeyEventArgs e)
    {
        base.OnKeyUp(e);

        if (this.TextLength == 0)
        {

        }
    }
}
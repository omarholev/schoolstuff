﻿namespace Övning_3._12
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labeledTextBos1 = new LabeledTextBox();
            this.SuspendLayout();
            // 
            // labeledTextBos1
            // 
            this.labeledTextBos1.ForeColor = System.Drawing.Color.Gray;
            this.labeledTextBos1.LedText = "AAAAA";
            this.labeledTextBos1.Location = new System.Drawing.Point(52, 38);
            this.labeledTextBos1.Name = "labeledTextBos1";
            this.labeledTextBos1.Size = new System.Drawing.Size(100, 20);
            this.labeledTextBos1.TabIndex = 0;
            this.labeledTextBos1.Text = "AAAAA";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labeledTextBos1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LabeledTextBos labeledTextBos1;
    }
}


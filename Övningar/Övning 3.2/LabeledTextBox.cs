﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;



class LabeledTextBox : TextBox
{
    private string ledText;
    [Category("Appearance"), Description("Label text."), DefaultValue("...")]

    public string LedText
    {
        get { return ledText; }
        set { ledText = value; Text = value;
            ForeColor = System.Drawing.Color.Gray;}


    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
        base.OnKeyDown(e);

        if (this.ForeColor == System.Drawing.Color.Gray)
        {
            this.Clear();
            this.ForeColor = System.Drawing.Color.Black;
        }
    }

    protected override void OnKeyUp(KeyEventArgs e)
    {
        base.OnKeyUp(e);

        if (this.Text.Length == 0)
        {
            this.Text = ledText;
            this.ForeColor = System.Drawing.Color.Gray;
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Övning_2_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int totalArea;

        private void btn_CreateTriangle_Click(object sender, EventArgs e)
        {
            int w = int.Parse(tbx_Width.Text);
            int h = int.Parse(tbx_Height.Text);
            
            Triangel triangel = new Triangel(w, h);

            int area = (triangel as IArea).CalculateArea(w, h);
            totalArea += area;
            lbx_ShapeList.Items.Add(triangel.ToString());

        }

        private void btn_CreateCircle_Click(object sender, EventArgs e)
        {
            int w = int.Parse(tbx_Width.Text);
            int h = int.Parse(tbx_Height.Text);

            Circle circle = new Circle(w, h);

            int area = (circle as IArea).CalculateArea(w, h);
            totalArea += area;
            lbx_ShapeList.Items.Add(circle.ToString());
        }

        private void btn_CreateLine_Click(object sender, EventArgs e)
        {
            int w = int.Parse(tbx_Width.Text);
            int h = int.Parse(tbx_Height.Text);

            Line line = new Line(w, h);

            int area = (line as IArea).CalculateArea(w, h);
            totalArea += area;
            lbx_ShapeList.Items.Add(line.ToString());
        }

        private void btn_SumArea_Click(object sender, EventArgs e)
        {
            tbx_SumArea.Text = totalArea.ToString();
        }
    }

    interface IArea
    {
        int CalculateArea(int w, int h);
    }
    class Shape
    {
        protected int w;
        protected int h;

        public Shape(int width, int height)
        {
            this.w = width;
            this.h = height;
        }
    }
    class Triangel : Shape, IArea
    {
        public Triangel(int width, int height) : base(width, height)
        {
            this.w = width;
            this.h = height;
        }
        int IArea.CalculateArea(int w, int h) { return (w * h / 2); }
        public override string ToString()
        {
            return ("Triangel: " + w + "x" + h);
        }
    }

    class Circle : Shape, IArea
    {
        public Circle(int width, int height) : base(width, height)
        {
            this.w = width;
            this.h = height;
        }

        int IArea.CalculateArea(int w, int h) { return (int)(Math.PI * (w/2) * (w/2)); }

        public override string ToString()
        {
            return ("Circle: " + w + "x" + h);
        }
    }

    class Line : Shape, IArea
    {
        public Line(int width, int height) : base(width, height)
        {
            this.w = width;
            this.h = height;
        }
        
        int IArea.CalculateArea(int w, int h) { return 0; }
        public override string ToString()
        {
            return ("Line: " + w + "x" + h);
        }

    }
}

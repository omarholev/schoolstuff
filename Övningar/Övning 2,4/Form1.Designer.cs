﻿namespace Övning_2_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_CreateTriangle = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_CreateLine = new System.Windows.Forms.Button();
            this.btn_CreateCircle = new System.Windows.Forms.Button();
            this.Height = new System.Windows.Forms.Label();
            this.tbx_Height = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbx_Width = new System.Windows.Forms.TextBox();
            this.lbx_ShapeList = new System.Windows.Forms.ListBox();
            this.btn_SumArea = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbx_SumArea = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_CreateTriangle
            // 
            this.btn_CreateTriangle.Location = new System.Drawing.Point(47, 75);
            this.btn_CreateTriangle.Name = "btn_CreateTriangle";
            this.btn_CreateTriangle.Size = new System.Drawing.Size(100, 23);
            this.btn_CreateTriangle.TabIndex = 0;
            this.btn_CreateTriangle.Text = "Create Triangle";
            this.btn_CreateTriangle.UseVisualStyleBackColor = true;
            this.btn_CreateTriangle.Click += new System.EventHandler(this.btn_CreateTriangle_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_CreateLine);
            this.groupBox1.Controls.Add(this.btn_CreateCircle);
            this.groupBox1.Controls.Add(this.Height);
            this.groupBox1.Controls.Add(this.tbx_Height);
            this.groupBox1.Controls.Add(this.btn_CreateTriangle);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbx_Width);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 166);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // btn_CreateLine
            // 
            this.btn_CreateLine.Location = new System.Drawing.Point(47, 133);
            this.btn_CreateLine.Name = "btn_CreateLine";
            this.btn_CreateLine.Size = new System.Drawing.Size(100, 23);
            this.btn_CreateLine.TabIndex = 7;
            this.btn_CreateLine.Text = "Create Line";
            this.btn_CreateLine.UseVisualStyleBackColor = true;
            this.btn_CreateLine.Click += new System.EventHandler(this.btn_CreateLine_Click);
            // 
            // btn_CreateCircle
            // 
            this.btn_CreateCircle.Location = new System.Drawing.Point(47, 104);
            this.btn_CreateCircle.Name = "btn_CreateCircle";
            this.btn_CreateCircle.Size = new System.Drawing.Size(100, 23);
            this.btn_CreateCircle.TabIndex = 6;
            this.btn_CreateCircle.Text = "Create Circle";
            this.btn_CreateCircle.UseVisualStyleBackColor = true;
            this.btn_CreateCircle.Click += new System.EventHandler(this.btn_CreateCircle_Click);
            // 
            // Height
            // 
            this.Height.AutoSize = true;
            this.Height.Location = new System.Drawing.Point(6, 52);
            this.Height.Name = "Height";
            this.Height.Size = new System.Drawing.Size(38, 13);
            this.Height.TabIndex = 5;
            this.Height.Text = "Height";
            // 
            // tbx_Height
            // 
            this.tbx_Height.Location = new System.Drawing.Point(47, 49);
            this.tbx_Height.Name = "tbx_Height";
            this.tbx_Height.Size = new System.Drawing.Size(100, 20);
            this.tbx_Height.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Width";
            // 
            // tbx_Width
            // 
            this.tbx_Width.Location = new System.Drawing.Point(47, 23);
            this.tbx_Width.Name = "tbx_Width";
            this.tbx_Width.Size = new System.Drawing.Size(100, 20);
            this.tbx_Width.TabIndex = 2;
            // 
            // lbx_ShapeList
            // 
            this.lbx_ShapeList.FormattingEnabled = true;
            this.lbx_ShapeList.Location = new System.Drawing.Point(193, 44);
            this.lbx_ShapeList.Name = "lbx_ShapeList";
            this.lbx_ShapeList.Size = new System.Drawing.Size(165, 134);
            this.lbx_ShapeList.TabIndex = 4;
            // 
            // btn_SumArea
            // 
            this.btn_SumArea.Location = new System.Drawing.Point(59, 199);
            this.btn_SumArea.Name = "btn_SumArea";
            this.btn_SumArea.Size = new System.Drawing.Size(100, 23);
            this.btn_SumArea.TabIndex = 8;
            this.btn_SumArea.Text = "Sum of Area";
            this.btn_SumArea.UseVisualStyleBackColor = true;
            this.btn_SumArea.Click += new System.EventHandler(this.btn_SumArea_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(193, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Shape list";
            // 
            // tbx_SumArea
            // 
            this.tbx_SumArea.Location = new System.Drawing.Point(193, 202);
            this.tbx_SumArea.Name = "tbx_SumArea";
            this.tbx_SumArea.ReadOnly = true;
            this.tbx_SumArea.Size = new System.Drawing.Size(165, 20);
            this.tbx_SumArea.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(193, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Sum of Area";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 279);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbx_SumArea);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_SumArea);
            this.Controls.Add(this.lbx_ShapeList);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_CreateTriangle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_CreateLine;
        private System.Windows.Forms.Button btn_CreateCircle;
        private System.Windows.Forms.Label Height;
        private System.Windows.Forms.TextBox tbx_Height;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_Width;
        private System.Windows.Forms.ListBox lbx_ShapeList;
        private System.Windows.Forms.Button btn_SumArea;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbx_SumArea;
        private System.Windows.Forms.Label label3;
    }
}


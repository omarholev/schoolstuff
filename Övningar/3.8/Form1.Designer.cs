﻿namespace _3._8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Bataljon");
            this.tvw_Batalion = new System.Windows.Forms.TreeView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbx_Name = new System.Windows.Forms.TextBox();
            this.tbx_NumSoldiers = new System.Windows.Forms.TextBox();
            this.btn_NewUnit = new System.Windows.Forms.Button();
            this.tbx_Out = new System.Windows.Forms.TextBox();
            this.btn_DeleteUnit = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvw_Batalion
            // 
            this.tvw_Batalion.Location = new System.Drawing.Point(28, 23);
            this.tvw_Batalion.Name = "tvw_Batalion";
            treeNode1.Name = "Bataljon";
            treeNode1.Text = "Bataljon";
            this.tvw_Batalion.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.tvw_Batalion.Size = new System.Drawing.Size(168, 234);
            this.tvw_Batalion.TabIndex = 0;
            this.tvw_Batalion.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvw_Batalion_AfterSelect);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_NewUnit);
            this.groupBox1.Controls.Add(this.tbx_NumSoldiers);
            this.groupBox1.Controls.Add(this.tbx_Name);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(214, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 118);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Lägg till ny enhet";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbx_Out);
            this.groupBox2.Location = new System.Drawing.Point(214, 147);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 110);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Information om vald enhet";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Namn:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Antal soldater:";
            // 
            // tbx_Name
            // 
            this.tbx_Name.Location = new System.Drawing.Point(94, 25);
            this.tbx_Name.Name = "tbx_Name";
            this.tbx_Name.Size = new System.Drawing.Size(100, 20);
            this.tbx_Name.TabIndex = 5;
            // 
            // tbx_NumSoldiers
            // 
            this.tbx_NumSoldiers.Location = new System.Drawing.Point(94, 54);
            this.tbx_NumSoldiers.Name = "tbx_NumSoldiers";
            this.tbx_NumSoldiers.Size = new System.Drawing.Size(100, 20);
            this.tbx_NumSoldiers.TabIndex = 6;
            // 
            // btn_NewUnit
            // 
            this.btn_NewUnit.Location = new System.Drawing.Point(94, 89);
            this.btn_NewUnit.Name = "btn_NewUnit";
            this.btn_NewUnit.Size = new System.Drawing.Size(100, 23);
            this.btn_NewUnit.TabIndex = 7;
            this.btn_NewUnit.Text = "Ny enhet";
            this.btn_NewUnit.UseVisualStyleBackColor = true;
            this.btn_NewUnit.Click += new System.EventHandler(this.btn_NewUnit_Click);
            // 
            // tbx_Out
            // 
            this.tbx_Out.Location = new System.Drawing.Point(9, 28);
            this.tbx_Out.Multiline = true;
            this.tbx_Out.Name = "tbx_Out";
            this.tbx_Out.ReadOnly = true;
            this.tbx_Out.Size = new System.Drawing.Size(185, 66);
            this.tbx_Out.TabIndex = 6;
            // 
            // btn_DeleteUnit
            // 
            this.btn_DeleteUnit.Location = new System.Drawing.Point(420, 23);
            this.btn_DeleteUnit.Name = "btn_DeleteUnit";
            this.btn_DeleteUnit.Size = new System.Drawing.Size(75, 23);
            this.btn_DeleteUnit.TabIndex = 8;
            this.btn_DeleteUnit.Text = "Ta bort";
            this.btn_DeleteUnit.UseVisualStyleBackColor = true;
            this.btn_DeleteUnit.Click += new System.EventHandler(this.btn_DeleteUnit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 348);
            this.Controls.Add(this.btn_DeleteUnit);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tvw_Batalion);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView tvw_Batalion;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_NewUnit;
        private System.Windows.Forms.TextBox tbx_NumSoldiers;
        private System.Windows.Forms.TextBox tbx_Name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbx_Out;
        private System.Windows.Forms.Button btn_DeleteUnit;
    }
}


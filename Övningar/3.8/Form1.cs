﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3._8
{
    public partial class Form1 : Form
    {
        const int BATALJON = 0, KOMPANI = 1, PLUTON = 2, GRUPP = 3;

        

        private string[] info = { "Bataljon", "Kompani", "Pluton", "Grupp" };

        private void btn_DeleteUnit_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = tvw_Batalion.SelectedNode;
            if (selectedNode != null)
            {
                selectedNode.Nodes.Remove(selectedNode);
            }
        }

        public Form1()
        {
            InitializeComponent();
            tvw_Batalion.SelectedNode = tvw_Batalion.Nodes[0];
        }


        private void btn_NewUnit_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = tvw_Batalion.SelectedNode;
            if (selectedNode != null)
            {
                TreeNode newNode = new TreeNode(tbx_Name.Text);
                selectedNode.Nodes.Add(newNode);
                if (newNode.Level == GRUPP) newNode.Tag = int.Parse(tbx_NumSoldiers.Text);
            }
        }

        private void tvw_Batalion_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode selectedNode = tvw_Batalion.SelectedNode;
            tbx_Out.Text = info[selectedNode.Level];
            tbx_Out.AppendText("\r\nNamn: " + selectedNode.Text);
            if (selectedNode.Level == GRUPP) groupBox1.Enabled = false;
            else groupBox2.Enabled = true;

            if (selectedNode.Level == PLUTON) tbx_NumSoldiers.Enabled = true;
            else tbx_NumSoldiers.Enabled = false;
        }
    }
}

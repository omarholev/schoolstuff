﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HängaGubbe
{
    public partial class HängaGubbjäveln : Form
    {
        string word;
        public StringBuilder revealingWord;



        public HängaGubbjäveln()
        {
            InitializeComponent();
            pbx_Gubbe.Top = 20;
        }

        private void btn_Play_Click(object sender, EventArgs e)
        {
            word = tbx_Word.Text;
            tbx_Word.PasswordChar = '?';

            string wordQuestionmarks = "";

            for (int i = 0; i < word.Length; i++)
            {
                wordQuestionmarks += "?";
            }

            revealingWord = new StringBuilder(wordQuestionmarks);
            tbx_RevealingWord.Text = revealingWord.ToString();
        }


        private void tbx_LetterInput_KeyUp(object sender, KeyEventArgs e)
        {
            Console.WriteLine(e.KeyCode);
            bool koolbool = false;
            char letter = tbx_LetterInput.Text[0];

            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] == letter)
                {
                    revealingWord.Remove(i, 1);
                    revealingWord.Insert(i, letter);
                    tbx_RevealingWord.Text = revealingWord.ToString();
                    koolbool = true;

                    if (!(revealingWord.ToString().Contains('?')))
                    {
                        MessageBox.Show("You Won!");
                    }
                }
            }
            if (!koolbool)
            {
                pbx_Gubbe.Top += 45;
                if (pbx_Gubbe.Bottom >= pbx_Ground.Bottom - 75)
                {
                    MessageBox.Show("You Lost!");
                }
            }

            tbx_LetterInput.Text = " ";
            return;
        }

        private void btn_PlayAgain_Click(object sender, EventArgs e)
        {
            word = "";
            tbx_Word.Clear();
            tbx_RevealingWord.Clear();
            pbx_Gubbe.Top = 20;
        }
    }  
}

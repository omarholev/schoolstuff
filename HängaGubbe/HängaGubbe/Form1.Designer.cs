﻿namespace HängaGubbe
{
    partial class HängaGubbjäveln
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HängaGubbjäveln));
            this.pbx_Gubbe = new System.Windows.Forms.PictureBox();
            this.pbx_Ground = new System.Windows.Forms.PictureBox();
            this.tbx_Word = new System.Windows.Forms.TextBox();
            this.tbx_LetterInput = new System.Windows.Forms.TextBox();
            this.tbx_RevealingWord = new System.Windows.Forms.TextBox();
            this.btn_Play = new System.Windows.Forms.Button();
            this.btn_PlayAgain = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_Gubbe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_Ground)).BeginInit();
            this.SuspendLayout();
            // 
            // pbx_Gubbe
            // 
            this.pbx_Gubbe.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.pbx_Gubbe.Image = ((System.Drawing.Image)(resources.GetObject("pbx_Gubbe.Image")));
            this.pbx_Gubbe.Location = new System.Drawing.Point(100, 10);
            this.pbx_Gubbe.Name = "pbx_Gubbe";
            this.pbx_Gubbe.Size = new System.Drawing.Size(100, 100);
            this.pbx_Gubbe.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_Gubbe.TabIndex = 0;
            this.pbx_Gubbe.TabStop = false;
            // 
            // pbx_Ground
            // 
            this.pbx_Ground.Image = ((System.Drawing.Image)(resources.GetObject("pbx_Ground.Image")));
            this.pbx_Ground.Location = new System.Drawing.Point(0, 400);
            this.pbx_Ground.Name = "pbx_Ground";
            this.pbx_Ground.Size = new System.Drawing.Size(600, 80);
            this.pbx_Ground.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_Ground.TabIndex = 1;
            this.pbx_Ground.TabStop = false;
            // 
            // tbx_Word
            // 
            this.tbx_Word.Location = new System.Drawing.Point(420, 40);
            this.tbx_Word.Name = "tbx_Word";
            this.tbx_Word.Size = new System.Drawing.Size(144, 20);
            this.tbx_Word.TabIndex = 2;
            // 
            // tbx_LetterInput
            // 
            this.tbx_LetterInput.Location = new System.Drawing.Point(450, 154);
            this.tbx_LetterInput.Name = "tbx_LetterInput";
            this.tbx_LetterInput.Size = new System.Drawing.Size(74, 20);
            this.tbx_LetterInput.TabIndex = 4;
            this.tbx_LetterInput.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbx_LetterInput_KeyUp);
            // 
            // tbx_RevealingWord
            // 
            this.tbx_RevealingWord.Location = new System.Drawing.Point(420, 117);
            this.tbx_RevealingWord.Name = "tbx_RevealingWord";
            this.tbx_RevealingWord.Size = new System.Drawing.Size(144, 20);
            this.tbx_RevealingWord.TabIndex = 5;
            // 
            // btn_Play
            // 
            this.btn_Play.Location = new System.Drawing.Point(450, 66);
            this.btn_Play.Name = "btn_Play";
            this.btn_Play.Size = new System.Drawing.Size(75, 23);
            this.btn_Play.TabIndex = 6;
            this.btn_Play.Text = "Play";
            this.btn_Play.UseVisualStyleBackColor = true;
            this.btn_Play.Click += new System.EventHandler(this.btn_Play_Click);
            // 
            // btn_PlayAgain
            // 
            this.btn_PlayAgain.Location = new System.Drawing.Point(420, 426);
            this.btn_PlayAgain.Name = "btn_PlayAgain";
            this.btn_PlayAgain.Size = new System.Drawing.Size(75, 23);
            this.btn_PlayAgain.TabIndex = 7;
            this.btn_PlayAgain.Text = "Play Again";
            this.btn_PlayAgain.UseVisualStyleBackColor = true;
            this.btn_PlayAgain.Click += new System.EventHandler(this.btn_PlayAgain_Click);
            // 
            // HängaGubbjäveln
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(584, 461);
            this.Controls.Add(this.btn_PlayAgain);
            this.Controls.Add(this.btn_Play);
            this.Controls.Add(this.tbx_RevealingWord);
            this.Controls.Add(this.tbx_LetterInput);
            this.Controls.Add(this.tbx_Word);
            this.Controls.Add(this.pbx_Ground);
            this.Controls.Add(this.pbx_Gubbe);
            this.Name = "HängaGubbjäveln";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pbx_Gubbe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_Ground)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbx_Gubbe;
        private System.Windows.Forms.PictureBox pbx_Ground;
        private System.Windows.Forms.TextBox tbx_Word;
        private System.Windows.Forms.TextBox tbx_LetterInput;
        private System.Windows.Forms.TextBox tbx_RevealingWord;
        private System.Windows.Forms.Button btn_Play;
        private System.Windows.Forms.Button btn_PlayAgain;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Uppgift6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        List<Suspect> suspects = new List<Suspect>();

        private void btn_Register_Click(object sender, EventArgs e)
        {
            List<string> suspectedCrimes = new List<string>(lbx_SuspectedCrime.Items.Cast<string>());
            suspects.Add(new Suspect(tbx_Name.Text, tbx_Gender.Text, 
                                     tbx_Age.Text, tbx_Height.Text, 
                                     tbx_Haircolor.Text, suspectedCrimes));

            MessageBox.Show("Brottsling Registrerad", "Avklarat", MessageBoxButtons.OK);
            tbx_Name.Clear();
            tbx_Gender.Clear();
            tbx_Age.Clear();
            tbx_Height.Clear();
            tbx_Haircolor.Clear();
            lbx_SuspectedCrime.Items.Clear();
            suspectedCrimes.Clear();

        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            List<int> indexes = new List<int>();
            bool succesfulSearch = false;
            for (int i = 0; i < suspects.Count(); i++ )
            {
                if (suspects[i].checkSearch(tbx_Search.Text))
                {
                    indexes.Add(i);
                    if (!succesfulSearch) succesfulSearch = true;
                }
            }

            if (succesfulSearch)
            {
                for (int i = 0; i < indexes.Count(); i++)

                    lbx_QuerieResult.Items.Add(suspects[indexes[i]].print() + "\n");
            }


        }
    }
}

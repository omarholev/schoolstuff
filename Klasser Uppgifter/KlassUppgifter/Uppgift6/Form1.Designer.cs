﻿namespace Uppgift6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbx_Anmälan = new System.Windows.Forms.GroupBox();
            this.lbl_SuspectedCrime = new System.Windows.Forms.Label();
            this.lbl_Suspect = new System.Windows.Forms.Label();
            this.lbl_Haircolor = new System.Windows.Forms.Label();
            this.lbl_Height = new System.Windows.Forms.Label();
            this.lbl_Age = new System.Windows.Forms.Label();
            this.lbl_Gender = new System.Windows.Forms.Label();
            this.lbl_Name = new System.Windows.Forms.Label();
            this.tbx_Haircolor = new System.Windows.Forms.TextBox();
            this.tbx_Height = new System.Windows.Forms.TextBox();
            this.tbx_Age = new System.Windows.Forms.TextBox();
            this.tbx_Gender = new System.Windows.Forms.TextBox();
            this.tbx_Name = new System.Windows.Forms.TextBox();
            this.lbx_SuspectedCrime = new System.Windows.Forms.ListBox();
            this.btn_Register = new System.Windows.Forms.Button();
            this.btn_ChangeMark = new System.Windows.Forms.Button();
            this.gbx_Search = new System.Windows.Forms.GroupBox();
            this.tbx_Search = new System.Windows.Forms.TextBox();
            this.lbl_Search = new System.Windows.Forms.Label();
            this.btn_Search = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbx_QuerieResult = new System.Windows.Forms.ListBox();
            this.btn_GetSelected = new System.Windows.Forms.Button();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.gbx_Anmälan.SuspendLayout();
            this.gbx_Search.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbx_Anmälan
            // 
            this.gbx_Anmälan.Controls.Add(this.lbl_SuspectedCrime);
            this.gbx_Anmälan.Controls.Add(this.lbl_Suspect);
            this.gbx_Anmälan.Controls.Add(this.lbl_Haircolor);
            this.gbx_Anmälan.Controls.Add(this.lbl_Height);
            this.gbx_Anmälan.Controls.Add(this.lbl_Age);
            this.gbx_Anmälan.Controls.Add(this.lbl_Gender);
            this.gbx_Anmälan.Controls.Add(this.lbl_Name);
            this.gbx_Anmälan.Controls.Add(this.tbx_Haircolor);
            this.gbx_Anmälan.Controls.Add(this.tbx_Height);
            this.gbx_Anmälan.Controls.Add(this.tbx_Age);
            this.gbx_Anmälan.Controls.Add(this.tbx_Gender);
            this.gbx_Anmälan.Controls.Add(this.tbx_Name);
            this.gbx_Anmälan.Controls.Add(this.lbx_SuspectedCrime);
            this.gbx_Anmälan.Controls.Add(this.btn_Register);
            this.gbx_Anmälan.Controls.Add(this.btn_ChangeMark);
            this.gbx_Anmälan.Location = new System.Drawing.Point(22, 28);
            this.gbx_Anmälan.Name = "gbx_Anmälan";
            this.gbx_Anmälan.Size = new System.Drawing.Size(395, 177);
            this.gbx_Anmälan.TabIndex = 0;
            this.gbx_Anmälan.TabStop = false;
            this.gbx_Anmälan.Text = "ANMÄLAN";
            // 
            // lbl_SuspectedCrime
            // 
            this.lbl_SuspectedCrime.AutoSize = true;
            this.lbl_SuspectedCrime.Location = new System.Drawing.Point(267, 16);
            this.lbl_SuspectedCrime.Name = "lbl_SuspectedCrime";
            this.lbl_SuspectedCrime.Size = new System.Drawing.Size(81, 13);
            this.lbl_SuspectedCrime.TabIndex = 21;
            this.lbl_SuspectedCrime.Text = "Brottsmisstanke";
            // 
            // lbl_Suspect
            // 
            this.lbl_Suspect.AutoSize = true;
            this.lbl_Suspect.Location = new System.Drawing.Point(7, 16);
            this.lbl_Suspect.Name = "lbl_Suspect";
            this.lbl_Suspect.Size = new System.Drawing.Size(69, 13);
            this.lbl_Suspect.TabIndex = 20;
            this.lbl_Suspect.Text = "Gärningsman";
            // 
            // lbl_Haircolor
            // 
            this.lbl_Haircolor.AutoSize = true;
            this.lbl_Haircolor.Location = new System.Drawing.Point(6, 144);
            this.lbl_Haircolor.Name = "lbl_Haircolor";
            this.lbl_Haircolor.Size = new System.Drawing.Size(42, 13);
            this.lbl_Haircolor.TabIndex = 19;
            this.lbl_Haircolor.Text = "Hårfärg";
            // 
            // lbl_Height
            // 
            this.lbl_Height.AutoSize = true;
            this.lbl_Height.Location = new System.Drawing.Point(7, 118);
            this.lbl_Height.Name = "lbl_Height";
            this.lbl_Height.Size = new System.Drawing.Size(37, 13);
            this.lbl_Height.TabIndex = 18;
            this.lbl_Height.Text = "Längd";
            // 
            // lbl_Age
            // 
            this.lbl_Age.AutoSize = true;
            this.lbl_Age.Location = new System.Drawing.Point(7, 92);
            this.lbl_Age.Name = "lbl_Age";
            this.lbl_Age.Size = new System.Drawing.Size(31, 13);
            this.lbl_Age.TabIndex = 17;
            this.lbl_Age.Text = "Ålder";
            // 
            // lbl_Gender
            // 
            this.lbl_Gender.AutoSize = true;
            this.lbl_Gender.Location = new System.Drawing.Point(7, 66);
            this.lbl_Gender.Name = "lbl_Gender";
            this.lbl_Gender.Size = new System.Drawing.Size(26, 13);
            this.lbl_Gender.TabIndex = 16;
            this.lbl_Gender.Text = "Kön";
            // 
            // lbl_Name
            // 
            this.lbl_Name.AutoSize = true;
            this.lbl_Name.Location = new System.Drawing.Point(6, 40);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(35, 13);
            this.lbl_Name.TabIndex = 15;
            this.lbl_Name.Text = "Namn";
            // 
            // tbx_Haircolor
            // 
            this.tbx_Haircolor.Location = new System.Drawing.Point(111, 141);
            this.tbx_Haircolor.Name = "tbx_Haircolor";
            this.tbx_Haircolor.Size = new System.Drawing.Size(153, 20);
            this.tbx_Haircolor.TabIndex = 14;
            // 
            // tbx_Height
            // 
            this.tbx_Height.Location = new System.Drawing.Point(111, 115);
            this.tbx_Height.Name = "tbx_Height";
            this.tbx_Height.Size = new System.Drawing.Size(153, 20);
            this.tbx_Height.TabIndex = 13;
            // 
            // tbx_Age
            // 
            this.tbx_Age.Location = new System.Drawing.Point(111, 89);
            this.tbx_Age.Name = "tbx_Age";
            this.tbx_Age.Size = new System.Drawing.Size(153, 20);
            this.tbx_Age.TabIndex = 12;
            // 
            // tbx_Gender
            // 
            this.tbx_Gender.Location = new System.Drawing.Point(111, 63);
            this.tbx_Gender.Name = "tbx_Gender";
            this.tbx_Gender.Size = new System.Drawing.Size(153, 20);
            this.tbx_Gender.TabIndex = 11;
            // 
            // tbx_Name
            // 
            this.tbx_Name.Location = new System.Drawing.Point(111, 37);
            this.tbx_Name.Name = "tbx_Name";
            this.tbx_Name.Size = new System.Drawing.Size(153, 20);
            this.tbx_Name.TabIndex = 10;
            // 
            // lbx_SuspectedCrime
            // 
            this.lbx_SuspectedCrime.FormattingEnabled = true;
            this.lbx_SuspectedCrime.Location = new System.Drawing.Point(270, 37);
            this.lbx_SuspectedCrime.Name = "lbx_SuspectedCrime";
            this.lbx_SuspectedCrime.Size = new System.Drawing.Size(119, 69);
            this.lbx_SuspectedCrime.TabIndex = 9;
            // 
            // btn_Register
            // 
            this.btn_Register.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Register.Location = new System.Drawing.Point(270, 141);
            this.btn_Register.Name = "btn_Register";
            this.btn_Register.Size = new System.Drawing.Size(119, 20);
            this.btn_Register.TabIndex = 5;
            this.btn_Register.Text = "Registrera";
            this.btn_Register.UseVisualStyleBackColor = true;
            this.btn_Register.Click += new System.EventHandler(this.btn_Register_Click);
            // 
            // btn_ChangeMark
            // 
            this.btn_ChangeMark.Location = new System.Drawing.Point(270, 115);
            this.btn_ChangeMark.Name = "btn_ChangeMark";
            this.btn_ChangeMark.Size = new System.Drawing.Size(119, 20);
            this.btn_ChangeMark.TabIndex = 4;
            this.btn_ChangeMark.Text = "Ändra Markerad";
            this.btn_ChangeMark.UseVisualStyleBackColor = true;
            // 
            // gbx_Search
            // 
            this.gbx_Search.Controls.Add(this.tbx_Search);
            this.gbx_Search.Controls.Add(this.lbl_Search);
            this.gbx_Search.Controls.Add(this.btn_Search);
            this.gbx_Search.Location = new System.Drawing.Point(22, 211);
            this.gbx_Search.Name = "gbx_Search";
            this.gbx_Search.Size = new System.Drawing.Size(395, 101);
            this.gbx_Search.TabIndex = 1;
            this.gbx_Search.TabStop = false;
            this.gbx_Search.Text = "SÖK";
            // 
            // tbx_Search
            // 
            this.tbx_Search.Location = new System.Drawing.Point(111, 31);
            this.tbx_Search.Name = "tbx_Search";
            this.tbx_Search.Size = new System.Drawing.Size(278, 20);
            this.tbx_Search.TabIndex = 8;
            // 
            // lbl_Search
            // 
            this.lbl_Search.AutoSize = true;
            this.lbl_Search.Location = new System.Drawing.Point(7, 31);
            this.lbl_Search.Name = "lbl_Search";
            this.lbl_Search.Size = new System.Drawing.Size(41, 13);
            this.lbl_Search.TabIndex = 7;
            this.lbl_Search.Text = "Sökord";
            // 
            // btn_Search
            // 
            this.btn_Search.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.Location = new System.Drawing.Point(270, 72);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(119, 23);
            this.btn_Search.TabIndex = 6;
            this.btn_Search.Text = "Sök";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(533, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "SÖKRESULTAT";
            // 
            // lbx_QuerieResult
            // 
            this.lbx_QuerieResult.FormattingEnabled = true;
            this.lbx_QuerieResult.Location = new System.Drawing.Point(423, 100);
            this.lbx_QuerieResult.Name = "lbx_QuerieResult";
            this.lbx_QuerieResult.Size = new System.Drawing.Size(297, 212);
            this.lbx_QuerieResult.TabIndex = 3;
            // 
            // btn_GetSelected
            // 
            this.btn_GetSelected.Location = new System.Drawing.Point(423, 65);
            this.btn_GetSelected.Name = "btn_GetSelected";
            this.btn_GetSelected.Size = new System.Drawing.Size(103, 23);
            this.btn_GetSelected.TabIndex = 7;
            this.btn_GetSelected.Text = "<- Hämta vald";
            this.btn_GetSelected.UseVisualStyleBackColor = true;
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(645, 65);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(75, 23);
            this.btn_Clear.TabIndex = 8;
            this.btn_Clear.Text = "Rensa";
            this.btn_Clear.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 347);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_GetSelected);
            this.Controls.Add(this.lbx_QuerieResult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbx_Search);
            this.Controls.Add(this.gbx_Anmälan);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbx_Anmälan.ResumeLayout(false);
            this.gbx_Anmälan.PerformLayout();
            this.gbx_Search.ResumeLayout(false);
            this.gbx_Search.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbx_Anmälan;
        private System.Windows.Forms.ListBox lbx_SuspectedCrime;
        private System.Windows.Forms.Button btn_Register;
        private System.Windows.Forms.Button btn_ChangeMark;
        private System.Windows.Forms.GroupBox gbx_Search;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbx_QuerieResult;
        private System.Windows.Forms.Button btn_GetSelected;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Label lbl_SuspectedCrime;
        private System.Windows.Forms.Label lbl_Suspect;
        private System.Windows.Forms.Label lbl_Haircolor;
        private System.Windows.Forms.Label lbl_Height;
        private System.Windows.Forms.Label lbl_Age;
        private System.Windows.Forms.Label lbl_Gender;
        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.TextBox tbx_Haircolor;
        private System.Windows.Forms.TextBox tbx_Height;
        private System.Windows.Forms.TextBox tbx_Age;
        private System.Windows.Forms.TextBox tbx_Gender;
        private System.Windows.Forms.TextBox tbx_Name;
        private System.Windows.Forms.TextBox tbx_Search;
        private System.Windows.Forms.Label lbl_Search;
    }
}


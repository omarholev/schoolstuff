﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uppgift6
{
    class Suspect
    {
        string name;
        string gender;
        string age;
        string height;
        string haircolor;

        List<string> crimes = new List<string>();

        public Suspect(string name, string gender, string age, string height, string haircolor, List<string> crimes)
        {
            this.name = name;
            this.gender = gender;
            this.age = age;
            this.height = height;
            this.haircolor = haircolor;
            this.crimes = crimes;
        }

        public string print()
        {
            return this.name + "; " + this.gender + "; " + this.age + "; " + this.height + "; " 
                    + this.haircolor + "; " + this.crimes.ToString();
        }

        public bool checkSearch(string prompt)
        {
            if (prompt == this.name || prompt == this.gender || prompt == this.age || prompt == this.height || prompt == this.haircolor )
            {
                return true;
            }

            for ( int i = 0; i < crimes.Count(); i++ )
            {
                if ( crimes[i] == prompt )
                {
                    return true;
                }
            }

            return false;
        }

       

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string Gender
        {
            get { return this.gender; }
            set { this.gender = value; }
        }
        public string Age
        {
            get { return this.age; }
            set { this.age = value; }
        }
        public string Height
        {
            get { return this.height; }
            set { this.height = value; }
        }
        public string HairColor
        {
            get { return this.haircolor; }
            set { this.haircolor = value; }
        }
        public List<string> Crimes
        {
            get { return this.crimes; }
            set { this.crimes = value; }
        }




    }
}

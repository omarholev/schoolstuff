﻿namespace KlasserUppgift1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbx_Namn = new System.Windows.Forms.TextBox();
            this.tbx_Poäng = new System.Windows.Forms.TextBox();
            this.lbx_Namn = new System.Windows.Forms.ListBox();
            this.gbx_Sök = new System.Windows.Forms.GroupBox();
            this.btn_Registrera = new System.Windows.Forms.Button();
            this.btn_VisaUrval = new System.Windows.Forms.Button();
            this.tbx_SökPoäng = new System.Windows.Forms.TextBox();
            this.lbl_Namn = new System.Windows.Forms.Label();
            this.lbl_Poäng = new System.Windows.Forms.Label();
            this.lbl_Poäng2 = new System.Windows.Forms.Label();
            this.gbx_Sök.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbx_Namn
            // 
            this.tbx_Namn.Location = new System.Drawing.Point(77, 41);
            this.tbx_Namn.Name = "tbx_Namn";
            this.tbx_Namn.Size = new System.Drawing.Size(100, 20);
            this.tbx_Namn.TabIndex = 0;
            // 
            // tbx_Poäng
            // 
            this.tbx_Poäng.Location = new System.Drawing.Point(77, 79);
            this.tbx_Poäng.Name = "tbx_Poäng";
            this.tbx_Poäng.Size = new System.Drawing.Size(100, 20);
            this.tbx_Poäng.TabIndex = 1;
            // 
            // lbx_Namn
            // 
            this.lbx_Namn.FormattingEnabled = true;
            this.lbx_Namn.Items.AddRange(new object[] {
            " "});
            this.lbx_Namn.Location = new System.Drawing.Point(6, 78);
            this.lbx_Namn.Name = "lbx_Namn";
            this.lbx_Namn.Size = new System.Drawing.Size(241, 121);
            this.lbx_Namn.TabIndex = 2;
            // 
            // gbx_Sök
            // 
            this.gbx_Sök.Controls.Add(this.lbl_Poäng2);
            this.gbx_Sök.Controls.Add(this.tbx_SökPoäng);
            this.gbx_Sök.Controls.Add(this.btn_VisaUrval);
            this.gbx_Sök.Controls.Add(this.lbx_Namn);
            this.gbx_Sök.Location = new System.Drawing.Point(13, 130);
            this.gbx_Sök.Name = "gbx_Sök";
            this.gbx_Sök.Size = new System.Drawing.Size(264, 205);
            this.gbx_Sök.TabIndex = 3;
            this.gbx_Sök.TabStop = false;
            this.gbx_Sök.Text = "Sök Poäng Över";
            // 
            // btn_Registrera
            // 
            this.btn_Registrera.Location = new System.Drawing.Point(183, 79);
            this.btn_Registrera.Name = "btn_Registrera";
            this.btn_Registrera.Size = new System.Drawing.Size(94, 23);
            this.btn_Registrera.TabIndex = 3;
            this.btn_Registrera.Text = "Registrera";
            this.btn_Registrera.UseVisualStyleBackColor = true;
            this.btn_Registrera.Click += new System.EventHandler(this.btn_Registrera_Click);
            // 
            // btn_VisaUrval
            // 
            this.btn_VisaUrval.Location = new System.Drawing.Point(92, 45);
            this.btn_VisaUrval.Name = "btn_VisaUrval";
            this.btn_VisaUrval.Size = new System.Drawing.Size(155, 23);
            this.btn_VisaUrval.TabIndex = 4;
            this.btn_VisaUrval.Text = "Visa Urval";
            this.btn_VisaUrval.UseVisualStyleBackColor = true;
            this.btn_VisaUrval.Click += new System.EventHandler(this.btn_VisaUrval_Click);
            // 
            // tbx_SökPoäng
            // 
            this.tbx_SökPoäng.Location = new System.Drawing.Point(92, 19);
            this.tbx_SökPoäng.Name = "tbx_SökPoäng";
            this.tbx_SökPoäng.Size = new System.Drawing.Size(155, 20);
            this.tbx_SökPoäng.TabIndex = 4;
            // 
            // lbl_Namn
            // 
            this.lbl_Namn.AutoSize = true;
            this.lbl_Namn.Location = new System.Drawing.Point(19, 47);
            this.lbl_Namn.Name = "lbl_Namn";
            this.lbl_Namn.Size = new System.Drawing.Size(35, 13);
            this.lbl_Namn.TabIndex = 4;
            this.lbl_Namn.Text = "Namn";
            // 
            // lbl_Poäng
            // 
            this.lbl_Poäng.AutoSize = true;
            this.lbl_Poäng.Location = new System.Drawing.Point(19, 82);
            this.lbl_Poäng.Name = "lbl_Poäng";
            this.lbl_Poäng.Size = new System.Drawing.Size(38, 13);
            this.lbl_Poäng.TabIndex = 5;
            this.lbl_Poäng.Text = "Poäng";
            // 
            // lbl_Poäng2
            // 
            this.lbl_Poäng2.AutoSize = true;
            this.lbl_Poäng2.Location = new System.Drawing.Point(6, 22);
            this.lbl_Poäng2.Name = "lbl_Poäng2";
            this.lbl_Poäng2.Size = new System.Drawing.Size(38, 13);
            this.lbl_Poäng2.TabIndex = 6;
            this.lbl_Poäng2.Text = "Poäng";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 366);
            this.Controls.Add(this.lbl_Poäng);
            this.Controls.Add(this.lbl_Namn);
            this.Controls.Add(this.btn_Registrera);
            this.Controls.Add(this.gbx_Sök);
            this.Controls.Add(this.tbx_Poäng);
            this.Controls.Add(this.tbx_Namn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbx_Sök.ResumeLayout(false);
            this.gbx_Sök.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbx_Namn;
        private System.Windows.Forms.TextBox tbx_Poäng;
        private System.Windows.Forms.ListBox lbx_Namn;
        private System.Windows.Forms.GroupBox gbx_Sök;
        private System.Windows.Forms.Label lbl_Poäng2;
        private System.Windows.Forms.TextBox tbx_SökPoäng;
        private System.Windows.Forms.Button btn_VisaUrval;
        private System.Windows.Forms.Button btn_Registrera;
        private System.Windows.Forms.Label lbl_Namn;
        private System.Windows.Forms.Label lbl_Poäng;
    }
}


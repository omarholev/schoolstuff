﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KlasserUppgift1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Registration> registrations = new List<Registration>();

        private void btn_Registrera_Click(object sender, EventArgs e)
        {

            if (int.Parse(tbx_Poäng.Text) <= 50) 
                {
                Registration newRegistration =  new Registration(tbx_Namn.Text, int.Parse(tbx_Poäng.Text));
               registrations.Add(newRegistration);
            }
            else
            {
                MessageBox.Show("Ogiltig poäng", "Felmeddelande", MessageBoxButtons.OK);
            }

            MessageBox.Show("Resultat Registrerat", "Klart", MessageBoxButtons.OK);
            tbx_Poäng.Clear();
            tbx_Namn.Clear();
        }

        private void btn_VisaUrval_Click(object sender, EventArgs e)
        {
            lbx_Namn.Items.Clear();
            List<string> namesAndScores = new List<string>();
            int pointSearch = int.Parse(tbx_SökPoäng.Text);
            for ( int i = 0; i < registrations.Count(); i++ )
            {
                if ( registrations[i].points >= pointSearch)
                {
                    string Output = registrations[i].name + ": " + registrations[i].points.ToString() + "\n";
                    namesAndScores.Add(Output);
                }
            }

            for ( int i = 0; i < namesAndScores.Count; i++ )
            {
                lbx_Namn.Items.Add(namesAndScores[i]);
            }
            
        }
    }
}

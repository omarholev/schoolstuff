﻿namespace Uppgift2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbx_Distance = new System.Windows.Forms.TextBox();
            this.tbx_Time = new System.Windows.Forms.TextBox();
            this.btn_Registrering = new System.Windows.Forms.Button();
            this.gbx_Searching = new System.Windows.Forms.GroupBox();
            this.lbl_Distance = new System.Windows.Forms.Label();
            this.lbl_Time = new System.Windows.Forms.Label();
            this.lbl_DistanceSearch = new System.Windows.Forms.Label();
            this.tbx_DistanceSearch = new System.Windows.Forms.TextBox();
            this.btn_ShowAllTimes = new System.Windows.Forms.Button();
            this.lbx_Output = new System.Windows.Forms.ListBox();
            this.btn_ShowAllSpeeds = new System.Windows.Forms.Button();
            this.gbx_Searching.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbx_Distance
            // 
            this.tbx_Distance.Location = new System.Drawing.Point(90, 12);
            this.tbx_Distance.Name = "tbx_Distance";
            this.tbx_Distance.Size = new System.Drawing.Size(112, 20);
            this.tbx_Distance.TabIndex = 0;
            // 
            // tbx_Time
            // 
            this.tbx_Time.Location = new System.Drawing.Point(90, 38);
            this.tbx_Time.Name = "tbx_Time";
            this.tbx_Time.Size = new System.Drawing.Size(112, 20);
            this.tbx_Time.TabIndex = 1;
            // 
            // btn_Registrering
            // 
            this.btn_Registrering.Location = new System.Drawing.Point(208, 38);
            this.btn_Registrering.Name = "btn_Registrering";
            this.btn_Registrering.Size = new System.Drawing.Size(60, 23);
            this.btn_Registrering.TabIndex = 2;
            this.btn_Registrering.Text = "Register";
            this.btn_Registrering.UseVisualStyleBackColor = true;
            this.btn_Registrering.Click += new System.EventHandler(this.btn_Registrering_Click);
            // 
            // gbx_Searching
            // 
            this.gbx_Searching.Controls.Add(this.btn_ShowAllSpeeds);
            this.gbx_Searching.Controls.Add(this.lbx_Output);
            this.gbx_Searching.Controls.Add(this.btn_ShowAllTimes);
            this.gbx_Searching.Controls.Add(this.tbx_DistanceSearch);
            this.gbx_Searching.Controls.Add(this.lbl_DistanceSearch);
            this.gbx_Searching.Location = new System.Drawing.Point(13, 74);
            this.gbx_Searching.Name = "gbx_Searching";
            this.gbx_Searching.Size = new System.Drawing.Size(261, 209);
            this.gbx_Searching.TabIndex = 3;
            this.gbx_Searching.TabStop = false;
            this.gbx_Searching.Text = "Sök på sträcka";
            // 
            // lbl_Distance
            // 
            this.lbl_Distance.AutoSize = true;
            this.lbl_Distance.Location = new System.Drawing.Point(12, 15);
            this.lbl_Distance.Name = "lbl_Distance";
            this.lbl_Distance.Size = new System.Drawing.Size(72, 13);
            this.lbl_Distance.TabIndex = 4;
            this.lbl_Distance.Text = "Distance (km)";
            // 
            // lbl_Time
            // 
            this.lbl_Time.AutoSize = true;
            this.lbl_Time.Location = new System.Drawing.Point(12, 41);
            this.lbl_Time.Name = "lbl_Time";
            this.lbl_Time.Size = new System.Drawing.Size(55, 13);
            this.lbl_Time.TabIndex = 5;
            this.lbl_Time.Text = "Time (min)";
            // 
            // lbl_DistanceSearch
            // 
            this.lbl_DistanceSearch.AutoSize = true;
            this.lbl_DistanceSearch.Location = new System.Drawing.Point(6, 24);
            this.lbl_DistanceSearch.Name = "lbl_DistanceSearch";
            this.lbl_DistanceSearch.Size = new System.Drawing.Size(72, 13);
            this.lbl_DistanceSearch.TabIndex = 0;
            this.lbl_DistanceSearch.Text = "Distance (km)";
            // 
            // tbx_DistanceSearch
            // 
            this.tbx_DistanceSearch.Location = new System.Drawing.Point(80, 21);
            this.tbx_DistanceSearch.Name = "tbx_DistanceSearch";
            this.tbx_DistanceSearch.Size = new System.Drawing.Size(175, 20);
            this.tbx_DistanceSearch.TabIndex = 1;
            // 
            // btn_ShowAllTimes
            // 
            this.btn_ShowAllTimes.Location = new System.Drawing.Point(80, 47);
            this.btn_ShowAllTimes.Name = "btn_ShowAllTimes";
            this.btn_ShowAllTimes.Size = new System.Drawing.Size(175, 23);
            this.btn_ShowAllTimes.TabIndex = 2;
            this.btn_ShowAllTimes.Text = "Show all times";
            this.btn_ShowAllTimes.UseVisualStyleBackColor = true;
            this.btn_ShowAllTimes.Click += new System.EventHandler(this.btn_ShowAllTimes_Click);
            // 
            // lbx_Output
            // 
            this.lbx_Output.FormattingEnabled = true;
            this.lbx_Output.Location = new System.Drawing.Point(6, 108);
            this.lbx_Output.Name = "lbx_Output";
            this.lbx_Output.Size = new System.Drawing.Size(249, 95);
            this.lbx_Output.TabIndex = 3;
            // 
            // btn_ShowAllSpeeds
            // 
            this.btn_ShowAllSpeeds.Location = new System.Drawing.Point(80, 76);
            this.btn_ShowAllSpeeds.Name = "btn_ShowAllSpeeds";
            this.btn_ShowAllSpeeds.Size = new System.Drawing.Size(175, 23);
            this.btn_ShowAllSpeeds.TabIndex = 4;
            this.btn_ShowAllSpeeds.Text = "Show all speeds";
            this.btn_ShowAllSpeeds.UseVisualStyleBackColor = true;
            this.btn_ShowAllSpeeds.Click += new System.EventHandler(this.btn_ShowAllSpeeds_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 317);
            this.Controls.Add(this.lbl_Time);
            this.Controls.Add(this.lbl_Distance);
            this.Controls.Add(this.gbx_Searching);
            this.Controls.Add(this.btn_Registrering);
            this.Controls.Add(this.tbx_Time);
            this.Controls.Add(this.tbx_Distance);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbx_Searching.ResumeLayout(false);
            this.gbx_Searching.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbx_Distance;
        private System.Windows.Forms.TextBox tbx_Time;
        private System.Windows.Forms.Button btn_Registrering;
        private System.Windows.Forms.GroupBox gbx_Searching;
        private System.Windows.Forms.Button btn_ShowAllSpeeds;
        private System.Windows.Forms.ListBox lbx_Output;
        private System.Windows.Forms.Button btn_ShowAllTimes;
        private System.Windows.Forms.TextBox tbx_DistanceSearch;
        private System.Windows.Forms.Label lbl_DistanceSearch;
        private System.Windows.Forms.Label lbl_Distance;
        private System.Windows.Forms.Label lbl_Time;
    }
}


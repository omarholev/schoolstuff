﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Uppgift2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Lap> laps = new List<Lap>();

        private void btn_Registrering_Click(object sender, EventArgs e)
        {
            Lap lap = new Lap(int.Parse(tbx_Distance.Text), int.Parse(tbx_Time.Text));
            laps.Add(lap);

            MessageBox.Show("Lap registered", "Confirmation", MessageBoxButtons.OK);
            tbx_Distance.Clear();
            tbx_Time.Clear();
        }

        

        private void btn_ShowAllTimes_Click(object sender, EventArgs e)
        {
            lbx_Output.Items.Clear();

            List<int> times = new List<int>();

            for ( int i = 0; i < laps.Count(); i++ )
            {
                if ( laps[i].distance == int.Parse(tbx_DistanceSearch.Text))
                {
                    times.Add(laps[i].time);
                }
            }

            for ( int i = 0; i < times.Count(); i++ )
            {
                lbx_Output.Items.Add(times[i].ToString() + " min \n");
            }

        }
        
        private void btn_ShowAllSpeeds_Click(object sender, EventArgs e)
        {
            lbx_Output.Items.Clear();

            List<int> speeds = new List<int>();

            for (int i = 0; i < laps.Count(); i++)
            {
                if (laps[i].distance == int.Parse(tbx_DistanceSearch.Text))
                {
                    speeds.Add((laps[i].distance * 1000) / (laps[i].time * 60));
                }
            }

            for (int i = 0; i < speeds.Count(); i++)
            {
                lbx_Output.Items.Add(speeds[i].ToString() + " m/s \n");
            }

        }
    }
}

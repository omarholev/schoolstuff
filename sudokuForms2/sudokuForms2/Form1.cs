﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sudokuForms2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int numSqaresPerSide = 9;
        Random rand = new Random();
        Square[] squares = new Square[81];



        private void removeEqualElements(ref List<int> list, List<int> listTool)
        {
            for (int i = 0; i <= listTool.Count() - 1; i++)
            {
                list.Remove(listTool[i]);
            }
        }

        private void removeIllegalNums(ref Square square)
        {

            List<int> rowValues = new List<int>();
            List<int> columnValues = new List<int>();
            List<int> bigSquareValues = new List<int>();

            int startingRow;
            int startingColumns;
            int startingSquare;


            int counter = 0;

            startingRow = square.yCor * 9;
            startingColumns = square.xCor;
            startingSquare = (square.xCor - (square.xCor % 3)) + ((square.yCor - (square.yCor % 3)) * 9);

            for (int i = startingRow; i <= startingRow + 8; i++)
            {
                rowValues.Add(squares[i].Value);
            }

            for (int i = startingColumns; i <= startingColumns + 9 * 8 - 1; i += 9)
            {
                columnValues.Add(squares[i].Value);
            }

            for (int i = startingSquare; i <= startingSquare + 20;)
            {
                bigSquareValues.Add(squares[i].Value);

                if (i % 3 == 2 && counter < 3)
                {
                    i += 9;
                    counter++;
                }

                else
                {
                    i++;
                }
            }

            removeEqualElements(ref square.allowedNums, rowValues);
            removeEqualElements(ref square.allowedNums, columnValues);
            removeEqualElements(ref square.allowedNums, bigSquareValues);

            Console.WriteLine("allowed nums = " + String.Join(" , ", square.allowedNums.ToArray()));
            



        }


        
        private void btn_Generate_Click(object sender, EventArgs e)
        {
            for ( int i = 0; i < 81; i++ )
            {
                squares[i] = new Square();
            }

            int n = 0;
            for (int i = 0; i < numSqaresPerSide; i++)
            {
                for (int j = 0; j < numSqaresPerSide; j++)
                {
                    
                    
                    squares[n].xCor = j;
                    squares[n].yCor = i;

                    removeIllegalNums(ref squares[n]);
                    Console.WriteLine("n = " + n);

                    squares[n].Value = squares[n].allowedNums[rand.Next(0, squares[n].allowedNums.Count())];
                    Console.WriteLine("value = " + squares[n].Value.ToString());


                    n++;
                }
            }


            for (int i = 0; i <= squares.Count() - 1; i++)
            {
                Console.WriteLine(squares[i].Value.ToString() + "   ");
                if (i % 9 == 8)
                {
                    Console.WriteLine("/n");
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prov_Programmering_2_Oliver
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Produkt> produkts = new List<Produkt>();
        private void btn_LäggTill_Click(object sender, EventArgs e)
        {
            string artikel = tbx_Artikel.Text;
            float vikt = 0;
            bool giltigProdukt = true;

            if (rbn_Bok.Checked || rbn_Kontorsmat.Checked)
            {
                vikt = float.Parse(tbx_Vikt.Text);
            }

            Produkt produkt;

            if (rbn_Bok.Checked) produkt = new Bok(artikel, vikt);
            else if (rbn_Kontorsmat.Checked) produkt = new Kontorsmaterial(artikel, vikt);
            else if (rbn_Ljudbok.Checked) produkt = new Ljudbok(artikel);
            else
            {
                giltigProdukt = false;
                produkt = null;
                MessageBox.Show("Vänligen välj en produkttyp");
            }
            if (giltigProdukt)
            {
                produkts.Add(produkt);
            }
        }

        private void btn_SkapaKvitto_Click(object sender, EventArgs e)
        {
            float totalVikt = 0;
            for (int i = 0; i < produkts.Count(); i++)
            {
                if (produkts[i] is Bok || produkts[i] is Kontorsmaterial)
                {
                    totalVikt += produkts[i].vikt;
                }
                lbx_Kvitto.Items.Add(produkts[i]);
            }

            tbx_TotalVikt.Text = totalVikt.ToString() + " kg";
        }
    }

    class Produkt
    {
        protected string artikel;
        public float vikt;
        public Produkt(string artikel)
        {
            this.artikel = artikel;
        }

        public override string ToString()
        {
            return "";
        }
    }
    class Ljudbok : Produkt
    {
        public Ljudbok(string artikel) : base(artikel)
        {
            this.artikel = artikel;
        }

        public override string ToString()
        {
            return artikel + ", Ljudbok";
        }

    }
    class Bok : Produkt
    {
        public Bok(string artikel, float vikt) : base(artikel)
        {
            this.artikel = artikel;
            this.vikt = vikt;
        }

        public override string ToString()
        {
            return artikel + ", Bok (" + vikt + " kg)";
        }
    }

    class Kontorsmaterial : Produkt
    {
        public Kontorsmaterial(string artikel, float vikt) : base(artikel)
        {
            this.artikel = artikel;
            this.vikt = vikt;
        }

        public override string ToString()
        {
            return artikel + ", Kontorsmaterial (" + vikt + " kg)";
        }
    }
}

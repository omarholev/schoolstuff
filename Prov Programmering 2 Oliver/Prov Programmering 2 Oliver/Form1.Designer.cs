﻿namespace Prov_Programmering_2_Oliver
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_LäggTill = new System.Windows.Forms.Button();
            this.tbx_Artikel = new System.Windows.Forms.TextBox();
            this.lbx_Kvitto = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbx_Vikt = new System.Windows.Forms.TextBox();
            this.rbn_Ljudbok = new System.Windows.Forms.RadioButton();
            this.rbn_Bok = new System.Windows.Forms.RadioButton();
            this.rbn_Kontorsmat = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbx_TotalVikt = new System.Windows.Forms.TextBox();
            this.btn_SkapaKvitto = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_LäggTill
            // 
            this.btn_LäggTill.Location = new System.Drawing.Point(86, 165);
            this.btn_LäggTill.Name = "btn_LäggTill";
            this.btn_LäggTill.Size = new System.Drawing.Size(75, 23);
            this.btn_LäggTill.TabIndex = 0;
            this.btn_LäggTill.Text = "Lägg Till";
            this.btn_LäggTill.UseVisualStyleBackColor = true;
            this.btn_LäggTill.Click += new System.EventHandler(this.btn_LäggTill_Click);
            // 
            // tbx_Artikel
            // 
            this.tbx_Artikel.Location = new System.Drawing.Point(76, 32);
            this.tbx_Artikel.Name = "tbx_Artikel";
            this.tbx_Artikel.Size = new System.Drawing.Size(100, 20);
            this.tbx_Artikel.TabIndex = 1;
            // 
            // lbx_Kvitto
            // 
            this.lbx_Kvitto.FormattingEnabled = true;
            this.lbx_Kvitto.Location = new System.Drawing.Point(210, 58);
            this.lbx_Kvitto.Name = "lbx_Kvitto";
            this.lbx_Kvitto.Size = new System.Drawing.Size(150, 82);
            this.lbx_Kvitto.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Artikel";
            // 
            // tbx_Vikt
            // 
            this.tbx_Vikt.Location = new System.Drawing.Point(76, 58);
            this.tbx_Vikt.Name = "tbx_Vikt";
            this.tbx_Vikt.Size = new System.Drawing.Size(100, 20);
            this.tbx_Vikt.TabIndex = 4;
            // 
            // rbn_Ljudbok
            // 
            this.rbn_Ljudbok.AutoSize = true;
            this.rbn_Ljudbok.Location = new System.Drawing.Point(76, 84);
            this.rbn_Ljudbok.Name = "rbn_Ljudbok";
            this.rbn_Ljudbok.Size = new System.Drawing.Size(63, 17);
            this.rbn_Ljudbok.TabIndex = 5;
            this.rbn_Ljudbok.TabStop = true;
            this.rbn_Ljudbok.Text = "Ljudbok";
            this.rbn_Ljudbok.UseVisualStyleBackColor = true;
            // 
            // rbn_Bok
            // 
            this.rbn_Bok.AutoSize = true;
            this.rbn_Bok.Location = new System.Drawing.Point(76, 110);
            this.rbn_Bok.Name = "rbn_Bok";
            this.rbn_Bok.Size = new System.Drawing.Size(44, 17);
            this.rbn_Bok.TabIndex = 6;
            this.rbn_Bok.TabStop = true;
            this.rbn_Bok.Text = "Bok";
            this.rbn_Bok.UseVisualStyleBackColor = true;
            // 
            // rbn_Kontorsmat
            // 
            this.rbn_Kontorsmat.AutoSize = true;
            this.rbn_Kontorsmat.Location = new System.Drawing.Point(76, 133);
            this.rbn_Kontorsmat.Name = "rbn_Kontorsmat";
            this.rbn_Kontorsmat.Size = new System.Drawing.Size(97, 17);
            this.rbn_Kontorsmat.TabIndex = 7;
            this.rbn_Kontorsmat.TabStop = true;
            this.rbn_Kontorsmat.Text = "Kontorsmaterial";
            this.rbn_Kontorsmat.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Vikt";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(207, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Totalvikt";
            // 
            // tbx_TotalVikt
            // 
            this.tbx_TotalVikt.Location = new System.Drawing.Point(260, 165);
            this.tbx_TotalVikt.Name = "tbx_TotalVikt";
            this.tbx_TotalVikt.ReadOnly = true;
            this.tbx_TotalVikt.Size = new System.Drawing.Size(100, 20);
            this.tbx_TotalVikt.TabIndex = 10;
            // 
            // btn_SkapaKvitto
            // 
            this.btn_SkapaKvitto.Location = new System.Drawing.Point(210, 27);
            this.btn_SkapaKvitto.Name = "btn_SkapaKvitto";
            this.btn_SkapaKvitto.Size = new System.Drawing.Size(150, 23);
            this.btn_SkapaKvitto.TabIndex = 11;
            this.btn_SkapaKvitto.Text = "Skapa Kvitto";
            this.btn_SkapaKvitto.UseVisualStyleBackColor = true;
            this.btn_SkapaKvitto.Click += new System.EventHandler(this.btn_SkapaKvitto_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 245);
            this.Controls.Add(this.btn_SkapaKvitto);
            this.Controls.Add(this.tbx_TotalVikt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbn_Kontorsmat);
            this.Controls.Add(this.rbn_Bok);
            this.Controls.Add(this.rbn_Ljudbok);
            this.Controls.Add(this.tbx_Vikt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbx_Kvitto);
            this.Controls.Add(this.tbx_Artikel);
            this.Controls.Add(this.btn_LäggTill);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_LäggTill;
        private System.Windows.Forms.TextBox tbx_Artikel;
        private System.Windows.Forms.ListBox lbx_Kvitto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_Vikt;
        private System.Windows.Forms.RadioButton rbn_Ljudbok;
        private System.Windows.Forms.RadioButton rbn_Bok;
        private System.Windows.Forms.RadioButton rbn_Kontorsmat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbx_TotalVikt;
        private System.Windows.Forms.Button btn_SkapaKvitto;
    }
}


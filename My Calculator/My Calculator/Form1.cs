﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace My_Calculator
{
    public partial class Form1 : Form
    {

        List<float> numbers = new List<float>();
        List<char> operators = new List<char>();



        public Form1()
        {
            InitializeComponent();
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += "1";
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += "2";
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += "3";
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += "4";
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += "5";
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += "6";
        }

        private void btn_7_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += "7";
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += "8";
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += "9";
        }

        private void btn_Dot_Click(object sender, EventArgs e)
        {
           // tbx_Input.Text += ".";
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += "0";
        }

        private void btn_Comma_Click(object sender, EventArgs e)
        {
            tbx_Input.Text += ",";
        }

        private void btn_Addition_Click(object sender, EventArgs e)
        {
            if (float.TryParse(tbx_Input.Text, out float result))
            {
                numbers.Add(float.Parse(tbx_Input.Text));
                tbx_Output.Text += tbx_Input.Text + "+";
            }
            
            else
            {
                tbx_Output.Text = "SYNTAX ERROR";
            }

            operators.Add('+');
            tbx_Input.Text = "";

        }

        private void btn_Subtraction_Click(object sender, EventArgs e)
        {
            if (numbers.Count != 0 && operators.Count != 0)
            {

                if (float.TryParse(tbx_Input.Text, out float result))
                {
                    operators.Add('-');
                    tbx_Output.Text += tbx_Input.Text + "-";
                    tbx_Input.Text = "";
                }

                else
                {
                    tbx_Output.Text = "SYNTAX ERROR";
                }
            }

            numbers.Add(float.Parse(tbx_Input.Text));
            tbx_Input.Text += "-";
        }

        private void btn_Multiplication_Click(object sender, EventArgs e)
        {
            if (float.TryParse(tbx_Input.Text, out float result))
            {
                numbers.Add(float.Parse(tbx_Input.Text));
                tbx_Output.Text += tbx_Input.Text + "*"; 
            }

            else
            {
                tbx_Output.Text = "SYNTAX ERROR";
            }
            operators.Add('*');
            tbx_Input.Text = "";
        }

        private void btn_Division_Click(object sender, EventArgs e)
        {
            if (float.TryParse(tbx_Input.Text, out float result))
            {
                numbers.Add(float.Parse(tbx_Input.Text));
                tbx_Output.Text += tbx_Input.Text + "/";
            }

            else
            {
                tbx_Output.Text = "SYNTAX ERROR";
            }
            operators.Add('/');
            tbx_Input.Text = "";
        }

        private void btn_Equals_Click(object sender, EventArgs e)
        {
            int numOperators = operators.Count;
            List<char> simpleOperators = new List<char>();

            if (float.TryParse(tbx_Input.Text, out float result))
            {
                numbers.Add(float.Parse(tbx_Input.Text));
                tbx_Output.Text += tbx_Input.Text + " = ";
            }

            else
            {
                tbx_Output.Text = "SYNTAX ERROR";
            }

            operators.Add('=');
            tbx_Input.Text = "";

            int i = 0;

            for (int j = 0; j < operators.Count; j++)
            {
                if (operators[i] == '*')
                {
                    numbers[i] *= numbers[i + 1];
                    operators.RemoveAt(i);
                    numbers.RemoveAt(i + 1);

                }
                
                else if (operators[i] == '/')
                {
                    numbers[i] /= numbers[i + 1];
                    operators.RemoveAt(i);
                    numbers.RemoveAt(i + 1);

                }

                else
                {
                    i++;
                }
            }

            numOperators = operators.Count;
            for (int x = 0; x < numbers.Count; x++)
            {
                if (operators[0] == '+')
                {
                    numbers[0] += numbers[1];
                    numbers.RemoveAt(1);
                    operators.RemoveAt(0);
                }
                else if (operators[0] == '-')
                {
                    numbers[0] -= numbers[1];
                    numbers.RemoveAt(1);
                    operators.RemoveAt(0);
                }
            }

            string Output = numbers[0].ToString();
            tbx_Output.Text += Output;

        }

        private void btn_ClearAll_Click(object sender, EventArgs e)
        {
            numbers.Clear();
            operators.Clear();
            tbx_Input.Text = "";
            tbx_Output.Text = "";

        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Candy_Machine
{
    public partial class Form1 : Form
    {
        const int numProducts = 6;
        List<double> boughtPrices = new List<double>();


        public Form1()
        {
            InitializeComponent();
        }
        
        private void btn_Köp_Click(object sender, EventArgs e)
        {
            bool[] isCheckboxChecked = new bool[numProducts] {cbx_BilarOrginal.Checked, cbx_BilarSaltlakrits.Checked, cbx_Loka.Checked, cbx_Punschrulle.Checked, cbx_Djungelvrål.Checked, cbx_Twix.Checked};
            double[] checkboxPrice = new double[numProducts] {double.Parse(cbx_BilarOrginal.Text), double.Parse(cbx_BilarSaltlakrits.Text), double.Parse(cbx_Loka.Text), double.Parse(cbx_Punschrulle.Text), double.Parse(cbx_Djungelvrål.Text), double.Parse(cbx_Twix.Text) };

            int payment = int.Parse(tbx_Money.Text);
            
            for (int i = 0; i < numProducts; i++)
            {
                if (isCheckboxChecked[i])
                {
                    boughtPrices.Add(checkboxPrice[i]);
                }
            }

            boughtPrices.Sort();
            
            int numFreeItems = boughtPrices.Count() / 3;
            if (numFreeItems >= 1)
            {
                for (int i = 0; i < numFreeItems; i++)
                {
                    boughtPrices.RemoveAt(0);
                }
            }
            

            double totalPrice = boughtPrices.Take(numProducts).Sum();
            tbx_Total.Text = totalPrice.ToString();

            if (payment >= totalPrice)
            {
                int change = payment - (int)Math.Round(totalPrice);
                int numTens = change / 10;
                int numOnes = change % 10;

                tbx_Num10Crowns.Text = numTens.ToString();
                tbx_NumCrowns.Text = numOnes.ToString();
            }

            else
            {
                tbx_Total.Text = "INVALID PAYMENT";
            }
        }

        
    }
}

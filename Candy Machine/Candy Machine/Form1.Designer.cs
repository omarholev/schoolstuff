﻿namespace Candy_Machine
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.cbx_BilarOrginal = new System.Windows.Forms.CheckBox();
            this.cbx_BilarSaltlakrits = new System.Windows.Forms.CheckBox();
            this.cbx_Loka = new System.Windows.Forms.CheckBox();
            this.cbx_Punschrulle = new System.Windows.Forms.CheckBox();
            this.cbx_Djungelvrål = new System.Windows.Forms.CheckBox();
            this.cbx_Twix = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbx_Money = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Köp = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbx_Num10Crowns = new System.Windows.Forms.TextBox();
            this.tbx_NumCrowns = new System.Windows.Forms.TextBox();
            this.tbx_Total = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.ErrorImage")));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(118, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(224, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(12, 116);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(118, 116);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 50);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(224, 116);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(100, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            // 
            // cbx_BilarOrginal
            // 
            this.cbx_BilarOrginal.AutoSize = true;
            this.cbx_BilarOrginal.Location = new System.Drawing.Point(13, 69);
            this.cbx_BilarOrginal.Name = "cbx_BilarOrginal";
            this.cbx_BilarOrginal.Size = new System.Drawing.Size(53, 17);
            this.cbx_BilarOrginal.TabIndex = 6;
            this.cbx_BilarOrginal.Text = "15,90";
            this.cbx_BilarOrginal.UseVisualStyleBackColor = true;
            // 
            // cbx_BilarSaltlakrits
            // 
            this.cbx_BilarSaltlakrits.AutoSize = true;
            this.cbx_BilarSaltlakrits.Location = new System.Drawing.Point(118, 68);
            this.cbx_BilarSaltlakrits.Name = "cbx_BilarSaltlakrits";
            this.cbx_BilarSaltlakrits.Size = new System.Drawing.Size(53, 17);
            this.cbx_BilarSaltlakrits.TabIndex = 7;
            this.cbx_BilarSaltlakrits.Text = "15,90";
            this.cbx_BilarSaltlakrits.UseVisualStyleBackColor = true;
            // 
            // cbx_Loka
            // 
            this.cbx_Loka.AutoSize = true;
            this.cbx_Loka.Location = new System.Drawing.Point(224, 68);
            this.cbx_Loka.Name = "cbx_Loka";
            this.cbx_Loka.Size = new System.Drawing.Size(47, 17);
            this.cbx_Loka.TabIndex = 8;
            this.cbx_Loka.Text = "9,90";
            this.cbx_Loka.UseVisualStyleBackColor = true;
            // 
            // cbx_Punschrulle
            // 
            this.cbx_Punschrulle.AutoSize = true;
            this.cbx_Punschrulle.Location = new System.Drawing.Point(12, 172);
            this.cbx_Punschrulle.Name = "cbx_Punschrulle";
            this.cbx_Punschrulle.Size = new System.Drawing.Size(47, 17);
            this.cbx_Punschrulle.TabIndex = 9;
            this.cbx_Punschrulle.Text = "7,90";
            this.cbx_Punschrulle.UseVisualStyleBackColor = true;
            // 
            // cbx_Djungelvrål
            // 
            this.cbx_Djungelvrål.AutoSize = true;
            this.cbx_Djungelvrål.Location = new System.Drawing.Point(118, 172);
            this.cbx_Djungelvrål.Name = "cbx_Djungelvrål";
            this.cbx_Djungelvrål.Size = new System.Drawing.Size(53, 17);
            this.cbx_Djungelvrål.TabIndex = 10;
            this.cbx_Djungelvrål.Text = "14,90";
            this.cbx_Djungelvrål.UseVisualStyleBackColor = true;
            // 
            // cbx_Twix
            // 
            this.cbx_Twix.AutoSize = true;
            this.cbx_Twix.Location = new System.Drawing.Point(224, 172);
            this.cbx_Twix.Name = "cbx_Twix";
            this.cbx_Twix.Size = new System.Drawing.Size(47, 17);
            this.cbx_Twix.TabIndex = 11;
            this.cbx_Twix.Text = "6,90";
            this.cbx_Twix.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 230);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Betalning";
            // 
            // tbx_Money
            // 
            this.tbx_Money.Location = new System.Drawing.Point(118, 227);
            this.tbx_Money.Name = "tbx_Money";
            this.tbx_Money.Size = new System.Drawing.Size(206, 20);
            this.tbx_Money.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Yellow;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(12, 278);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(187, 15);
            this.label2.TabIndex = 14;
            this.label2.Text = "Köp 3 och få den billigaste gratis!";
            // 
            // btn_Köp
            // 
            this.btn_Köp.Location = new System.Drawing.Point(209, 267);
            this.btn_Köp.Name = "btn_Köp";
            this.btn_Köp.Size = new System.Drawing.Size(114, 71);
            this.btn_Köp.TabIndex = 15;
            this.btn_Köp.Text = "Köp";
            this.btn_Köp.UseVisualStyleBackColor = true;
            this.btn_Köp.Click += new System.EventHandler(this.btn_Köp_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(111, 355);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Växel, antal tior";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(111, 401);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Växel, antal enkronor";
            // 
            // tbx_Num10Crowns
            // 
            this.tbx_Num10Crowns.Location = new System.Drawing.Point(224, 352);
            this.tbx_Num10Crowns.Name = "tbx_Num10Crowns";
            this.tbx_Num10Crowns.ReadOnly = true;
            this.tbx_Num10Crowns.Size = new System.Drawing.Size(100, 20);
            this.tbx_Num10Crowns.TabIndex = 18;
            // 
            // tbx_NumCrowns
            // 
            this.tbx_NumCrowns.Location = new System.Drawing.Point(224, 398);
            this.tbx_NumCrowns.Name = "tbx_NumCrowns";
            this.tbx_NumCrowns.ReadOnly = true;
            this.tbx_NumCrowns.Size = new System.Drawing.Size(100, 20);
            this.tbx_NumCrowns.TabIndex = 19;
            // 
            // tbx_Total
            // 
            this.tbx_Total.Location = new System.Drawing.Point(222, 201);
            this.tbx_Total.Name = "tbx_Total";
            this.tbx_Total.ReadOnly = true;
            this.tbx_Total.Size = new System.Drawing.Size(100, 20);
            this.tbx_Total.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(179, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Total :";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 469);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbx_Total);
            this.Controls.Add(this.tbx_NumCrowns);
            this.Controls.Add(this.tbx_Num10Crowns);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_Köp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbx_Money);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbx_Twix);
            this.Controls.Add(this.cbx_Djungelvrål);
            this.Controls.Add(this.cbx_Punschrulle);
            this.Controls.Add(this.cbx_Loka);
            this.Controls.Add(this.cbx_BilarSaltlakrits);
            this.Controls.Add(this.cbx_BilarOrginal);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.CheckBox cbx_BilarOrginal;
        private System.Windows.Forms.CheckBox cbx_BilarSaltlakrits;
        private System.Windows.Forms.CheckBox cbx_Loka;
        private System.Windows.Forms.CheckBox cbx_Punschrulle;
        private System.Windows.Forms.CheckBox cbx_Djungelvrål;
        private System.Windows.Forms.CheckBox cbx_Twix;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_Money;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Köp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbx_Num10Crowns;
        private System.Windows.Forms.TextBox tbx_NumCrowns;
        private System.Windows.Forms.TextBox tbx_Total;
        private System.Windows.Forms.Label label5;
    }
}


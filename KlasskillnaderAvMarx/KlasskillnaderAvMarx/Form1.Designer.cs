﻿namespace KlasskillnaderAvMarx
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbx_Kund = new System.Windows.Forms.TextBox();
            this.tbx_Destination = new System.Windows.Forms.TextBox();
            this.tbx_AntalDagar = new System.Windows.Forms.TextBox();
            this.lbl_Kund = new System.Windows.Forms.Label();
            this.lbl_Destination = new System.Windows.Forms.Label();
            this.lbl_AntalDagar = new System.Windows.Forms.Label();
            this.btn_Boka = new System.Windows.Forms.Button();
            this.gbx_Sök = new System.Windows.Forms.GroupBox();
            this.btn_Sök = new System.Windows.Forms.Button();
            this.lbx_Bokningar = new System.Windows.Forms.ListBox();
            this.tbx_Namn = new System.Windows.Forms.TextBox();
            this.lbl_Namn = new System.Windows.Forms.Label();
            this.gbx_Sök.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbx_Kund
            // 
            this.tbx_Kund.Location = new System.Drawing.Point(104, 44);
            this.tbx_Kund.Name = "tbx_Kund";
            this.tbx_Kund.Size = new System.Drawing.Size(100, 20);
            this.tbx_Kund.TabIndex = 0;
            // 
            // tbx_Destination
            // 
            this.tbx_Destination.Location = new System.Drawing.Point(104, 79);
            this.tbx_Destination.Name = "tbx_Destination";
            this.tbx_Destination.Size = new System.Drawing.Size(100, 20);
            this.tbx_Destination.TabIndex = 1;
            // 
            // tbx_AntalDagar
            // 
            this.tbx_AntalDagar.Location = new System.Drawing.Point(104, 123);
            this.tbx_AntalDagar.Name = "tbx_AntalDagar";
            this.tbx_AntalDagar.Size = new System.Drawing.Size(100, 20);
            this.tbx_AntalDagar.TabIndex = 2;
            // 
            // lbl_Kund
            // 
            this.lbl_Kund.AutoSize = true;
            this.lbl_Kund.Location = new System.Drawing.Point(35, 47);
            this.lbl_Kund.Name = "lbl_Kund";
            this.lbl_Kund.Size = new System.Drawing.Size(32, 13);
            this.lbl_Kund.TabIndex = 3;
            this.lbl_Kund.Text = "Kund";
            // 
            // lbl_Destination
            // 
            this.lbl_Destination.AutoSize = true;
            this.lbl_Destination.Location = new System.Drawing.Point(35, 82);
            this.lbl_Destination.Name = "lbl_Destination";
            this.lbl_Destination.Size = new System.Drawing.Size(60, 13);
            this.lbl_Destination.TabIndex = 4;
            this.lbl_Destination.Text = "Destination";
            // 
            // lbl_AntalDagar
            // 
            this.lbl_AntalDagar.AutoSize = true;
            this.lbl_AntalDagar.Location = new System.Drawing.Point(35, 123);
            this.lbl_AntalDagar.Name = "lbl_AntalDagar";
            this.lbl_AntalDagar.Size = new System.Drawing.Size(63, 13);
            this.lbl_AntalDagar.TabIndex = 5;
            this.lbl_AntalDagar.Text = "Antal Dagar";
            // 
            // btn_Boka
            // 
            this.btn_Boka.Location = new System.Drawing.Point(210, 123);
            this.btn_Boka.Name = "btn_Boka";
            this.btn_Boka.Size = new System.Drawing.Size(75, 23);
            this.btn_Boka.TabIndex = 6;
            this.btn_Boka.Text = "Boka";
            this.btn_Boka.UseVisualStyleBackColor = true;
            this.btn_Boka.Click += new System.EventHandler(this.btn_Boka_Click);
            // 
            // gbx_Sök
            // 
            this.gbx_Sök.Controls.Add(this.btn_Sök);
            this.gbx_Sök.Controls.Add(this.lbx_Bokningar);
            this.gbx_Sök.Controls.Add(this.tbx_Namn);
            this.gbx_Sök.Controls.Add(this.lbl_Namn);
            this.gbx_Sök.Location = new System.Drawing.Point(38, 190);
            this.gbx_Sök.Name = "gbx_Sök";
            this.gbx_Sök.Size = new System.Drawing.Size(257, 192);
            this.gbx_Sök.TabIndex = 7;
            this.gbx_Sök.TabStop = false;
            this.gbx_Sök.Text = "Sök Kund";
            // 
            // btn_Sök
            // 
            this.btn_Sök.Location = new System.Drawing.Point(172, 23);
            this.btn_Sök.Name = "btn_Sök";
            this.btn_Sök.Size = new System.Drawing.Size(75, 23);
            this.btn_Sök.TabIndex = 8;
            this.btn_Sök.Text = "Sök";
            this.btn_Sök.UseVisualStyleBackColor = true;
            this.btn_Sök.Click += new System.EventHandler(this.btn_Sök_Click);
            // 
            // lbx_Bokningar
            // 
            this.lbx_Bokningar.FormattingEnabled = true;
            this.lbx_Bokningar.Items.AddRange(new object[] {
            " "});
            this.lbx_Bokningar.Location = new System.Drawing.Point(9, 64);
            this.lbx_Bokningar.Name = "lbx_Bokningar";
            this.lbx_Bokningar.Size = new System.Drawing.Size(238, 108);
            this.lbx_Bokningar.TabIndex = 9;
            // 
            // tbx_Namn
            // 
            this.tbx_Namn.Location = new System.Drawing.Point(66, 26);
            this.tbx_Namn.Name = "tbx_Namn";
            this.tbx_Namn.Size = new System.Drawing.Size(100, 20);
            this.tbx_Namn.TabIndex = 8;
            // 
            // lbl_Namn
            // 
            this.lbl_Namn.AutoSize = true;
            this.lbl_Namn.Location = new System.Drawing.Point(6, 28);
            this.lbl_Namn.Name = "lbl_Namn";
            this.lbl_Namn.Size = new System.Drawing.Size(35, 13);
            this.lbl_Namn.TabIndex = 8;
            this.lbl_Namn.Text = "Namn";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 448);
            this.Controls.Add(this.gbx_Sök);
            this.Controls.Add(this.btn_Boka);
            this.Controls.Add(this.lbl_AntalDagar);
            this.Controls.Add(this.lbl_Destination);
            this.Controls.Add(this.lbl_Kund);
            this.Controls.Add(this.tbx_AntalDagar);
            this.Controls.Add(this.tbx_Destination);
            this.Controls.Add(this.tbx_Kund);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbx_Sök.ResumeLayout(false);
            this.gbx_Sök.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbx_Kund;
        private System.Windows.Forms.TextBox tbx_Destination;
        private System.Windows.Forms.TextBox tbx_AntalDagar;
        private System.Windows.Forms.Label lbl_Kund;
        private System.Windows.Forms.Label lbl_Destination;
        private System.Windows.Forms.Label lbl_AntalDagar;
        private System.Windows.Forms.Button btn_Boka;
        private System.Windows.Forms.GroupBox gbx_Sök;
        private System.Windows.Forms.Button btn_Sök;
        private System.Windows.Forms.ListBox lbx_Bokningar;
        private System.Windows.Forms.TextBox tbx_Namn;
        private System.Windows.Forms.Label lbl_Namn;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlasskillnaderAvMarx
{
    class Bokning
    {

        // variables
        public string name;
        public List<string> location = new List<string>();
        public List<int> numDays = new List<int>();


        // constructor
        public Bokning ( string name, string location, int numDays )
        {
            this.name = name;
            this.location.Add(location);
            this.numDays.Add(numDays);
        }

        public void addNew(string location, int numDays)
        {
            this.location.Add(location);
            this.numDays.Add(numDays);
        }

        // methods
        public void outputFunc(System.Windows.Forms.ListBox listbox)
        {
            for ( int i = 0; i < location.Count(); i++ )
            {
                string output = location[i] + ": " + numDays[i] + " dagar \n";
                listbox.Items.Add(output);

            }
        }
    }
}

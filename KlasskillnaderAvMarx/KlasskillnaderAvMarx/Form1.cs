﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KlasskillnaderAvMarx
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Bokning> bokningar = new List<Bokning>();

        private void btn_Boka_Click(object sender, EventArgs e)
        {
            bool isReaccuringUser = false;
            int index = 0;
            for ( int i = 0; i < bokningar.Count(); i++ )
            {
                if (bokningar[i].name == tbx_Kund.Text)
                {
                    isReaccuringUser = true;
                    index = i;
                }
            }
            
            if (isReaccuringUser)
            {
                bokningar[index].addNew(tbx_Destination.Text, int.Parse(tbx_AntalDagar.Text));
                DialogResult Bekräftelse;
                Bekräftelse = MessageBox.Show("Resan Bokad", "Bekräftelse", MessageBoxButtons.OK);
            }
            else
            {
                bokningar.Add(new Bokning(tbx_Kund.Text, tbx_Destination.Text, int.Parse(tbx_AntalDagar.Text)));
                DialogResult Bekräftelse;
                Bekräftelse = MessageBox.Show("Resan Bokad", "Bekräftelse", MessageBoxButtons.OK);
            }

            tbx_AntalDagar.Clear();
            tbx_Destination.Clear();
            tbx_Kund.Clear();
            tbx_Namn.Clear();
            lbx_Bokningar.Items.Clear();
        }

        private void btn_Sök_Click(object sender, EventArgs e)
        {
            string name = tbx_Namn.Text;
            int i;
            for ( i = 0;  i < bokningar.Count(); i++ )
            {
                if ( bokningar[i].name == name)
                {
                    break;
                }
                else
                {
                    continue;
                }
            }

            bokningar[i].outputFunc(lbx_Bokningar);

            
        }
    }
}

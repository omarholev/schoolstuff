﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Primtalsfaktorisering
{
    public partial class Form1 : Form
    {

        List<int> primes = GeneratePrimesNaive(99999);
        
        public Form1()
        {
            InitializeComponent();
        }

        public static List<int> GeneratePrimesNaive(int n)
        {
            List<int> primes = new List<int>();
            primes.Add(2);
            int nextPrime = 3;
            while (primes.Count < n)
            {
                int sqrt = (int)Math.Sqrt(nextPrime);
                bool isPrime = true;
                for (int i = 0; (int)primes[i] <= sqrt; i++)
                {
                    if (nextPrime % primes[i] == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime)
                {
                    primes.Add(nextPrime);
                }
                nextPrime += 2;
            }
            return primes;
        }


        private void btn_Run_Click(object sender, EventArgs e)
        {
            
            List<int> multiplicand = new List<int>();
            int inputNum = int.Parse(tbx_Input.Text);
            int inputNumSaved = inputNum;
            int productOfList = 1;
            int b = 0;
            int i = 0;


            //Loopa igenom detta utan att skippa element i listan
            while (productOfList <= inputNumSaved)
            {
                //Testa ifall primes[b] är en faktor i talet inputNum   
                if (((float)inputNum % primes[b]) == 0)
                {
                    //Ifall ja -> lägg till primes[b] i multiplicand
                    multiplicand.Add(primes[b]);
                    inputNum /= primes[b];

                    //Multiplicera alla tal i multiplicand och se ifall det stämmer med inputNum
                    if (multiplicand.Count() > 1)
                    {
                        productOfList *= multiplicand[i];
                        i++;
                    }
                }
                else
                {
                    b++;
                }

                

                if (primes[b] > inputNum)
                {
                    break;
                }



            }

            string Output = string.Join(", ", multiplicand.ToArray());
            if (multiplicand.Count() == 1)
            {
                Output = "Talet är ett primtal";
            }
            tbx_Output.Text = Output;

        }
    }
}
